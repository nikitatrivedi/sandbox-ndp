package ndp.esdb;
import java.io.IOException;

import org.apache.log4j.Logger;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.IndicesExists;

public class EsdbClient {
	private static final Logger log = Logger.getLogger(EsdbClient.class);
	private JestClient esclient ;
	private static String dbip = "";
	private static String dbport="";
	
	
	public EsdbClient(String ip, String port) {
		dbip = ip;
		dbport = port;
		this.esclient = jestClient();
	}
	public JestClient jestClient() {
	    JestClientFactory factory = new JestClientFactory();
	    factory.setHttpClientConfig(
	      new HttpClientConfig.Builder("http://"+dbip+":"+dbport)//"172.30.156.91:9200")
	        .multiThreaded(true)
	        .defaultMaxTotalConnectionPerRoute(2)
	        .maxTotalConnection(10)
	        .build());
	    return factory.getObject();
	}

	public void createIndex(String indexname) {
		boolean indexExists;
		try {
			indexExists = esclient.execute(new IndicesExists.Builder(indexname).build()).isSucceeded();
			 if (indexExists) {
		            esclient.execute(new DeleteIndex.Builder(indexname).build());
		        }
				esclient.execute(new CreateIndex.Builder(indexname).build());
				

		} catch (IOException e) {
			System.out.println("Exception occured while creating Index");
		}
       
	}
	
	public boolean putData( String indexname, String id, String data) {
		try {
			DocumentResult res  = esclient.execute(new Index.Builder(data).index(indexname).type("_doc").id(id).build());
			System.out.println("Data stored " + res.getJsonString());
			log.info("Data stored " + res.getJsonString());

			return true;
		} catch (IOException e) {
			System.out.println("Exception occured while insering data in db" + e.getLocalizedMessage());
			log.debug("Exception occured while insering data in db" + e.getLocalizedMessage());
			return false;
		}

	}
	
	public String fetchData(String dbindex, String docid) {
		String record = "";
		try {
			record = this.esclient.execute(new Get.Builder(dbindex, docid).build()).getJsonString();
			//.getSourceAsObject(typeclass.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Exception occured in fetch data" + e.getLocalizedMessage());		
		}
		return record;
	}
	
	public String matchQuery(String search, String index) {
		
		JestResult result;
		String resultstr="na";
		try {
			result = this.esclient.execute(new Search.Builder(search).addIndex(index).build());
			resultstr = result.getJsonString();
		} catch (IOException e) {
			System.out.println("Exception occured in match query" + e.getLocalizedMessage());		
}
		return resultstr;
	}
}
