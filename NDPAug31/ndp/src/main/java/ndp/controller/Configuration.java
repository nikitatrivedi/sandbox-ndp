package ndp.controller;

import java.text.ParseException;
import java.time.Instant;

public class Configuration {
	//private static Logger log = Logger.getLogger(Configuration.class);
	private String usecase = "";
	private String filepath = "";
	private String topotimestamp;
	private String dbip;
	private String dbport;
	private String datageneratorip;
	private String datageneratorport;

	public String getUsecase() {
		return usecase;
	}

	public void setUsecase(String usecase) {
		this.usecase = usecase;
	}

	public Configuration() {
		System.out.println("Configuration class constructor called !!!");
		this.topotimestamp = Instant.now().toString();
	}
	

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getDbip() {
		return dbip;
	}

	public void setDbip(String dbip) {
		this.dbip = dbip;
	}

	public String getDbport() {
		return dbport;
	}

	public void setDbport(String dbport) {
		this.dbport = dbport;
	}

	public String getDatageneratorip() {
		return datageneratorip;
	}

	public void setDatageneratorip(String datageneratorip) {
		this.datageneratorip = datageneratorip;
	}
	
	public String getDatageneratorport() {
		return datageneratorport;
	}

	public void setDatageneratorport(String datageneratorport) {
		this.datageneratorport = datageneratorport;
	}

	public String getTopotimestamp() {
		return topotimestamp;
	}

	public void setTopotimestamp(String topotimestamp) {
		
		try {
			this.topotimestamp = modifyDateLayout(topotimestamp);
		} catch (ParseException e) {
			System.out.println("Exception occured while parsing topotimestamp"+ e.getLocalizedMessage());
		}
	}
	
	private String modifyDateLayout(String inputDate) throws ParseException{
		   Instant timestamp = Instant.parse(inputDate);
		   return timestamp.toString();
		}

	@Override public boolean equals(Object obj)
    {
 
        // checking if the two objects
        // pointing to same object
        if (this == obj)
            return true;
 
        // checking for two condition:
        // 1) object is pointing to null
        // 2) if the objects belong to
        // same class or not
        if (obj == null
            || this.getClass() != obj.getClass())
            return false;
 
        Configuration p1 = (Configuration)obj; // type casting object to the
                           // intended class type
 
        // checking if the two
        // objects share all the same values
        return this.usecase.equals(p1.usecase)
            && this.filepath.equals(p1.filepath)
            && this.dbip.equals(p1.dbip)
        	&& this.dbport.equals(p1.dbport);

    }
	
}
