package ndp.controller;

public class dataGeneratorInputConfig {
	private String netId;
	private int rop;
	private String usecase;
	private String starttime;
	private String stoptime; 	// added on July 18
	private int srop;
	private String topologyJson;
	public int getRop() {
		return rop;
	}
	public void setRop(int rop) {
		this.rop = rop;
	}
	public String getUsecase() {
		return usecase;
	}
	public void setUsecase(String usecase) {
		this.usecase = usecase;
	}

	public String getTopologyJson() {
		return topologyJson;
	}
	public void setTopologyJson(String filepath) {
		this.topologyJson = filepath;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getNetId() {
		return netId;
	}
	public void setNetId(String netId) {
		this.netId = netId;
	}
	public String getStoptime() {
		return stoptime;
	}
	public void setStoptime(String stoptime) {
		this.stoptime = stoptime;
	}
	public int getSrop() {
		return srop;
	}
	public void setSrop(int srop) {
		this.srop = srop;
	}
	
}
