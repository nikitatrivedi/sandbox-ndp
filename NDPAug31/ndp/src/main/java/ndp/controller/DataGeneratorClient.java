package ndp.controller;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DataGeneratorClient {
	private static final Logger log = Logger.getLogger(DataGeneratorClient.class);

	private String initiateurl = "";
	private String datageneratorResponse="";
	private String networkTopology="";
	private String networkId="";
	private Configuration config;
	private dataGeneratorInputConfig dginput;
	private String dgip;
	private String dgport;

	private String kpiurl;
	
	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getNetworkTopology() {
		return networkTopology;
	}

	public void setNetworkTopology(String networkTopology) {
		this.networkTopology = networkTopology;
	}

	public String getDatageneratorResponse() {
		return datageneratorResponse;
	}

	public void setDatageneratorResponse(String datageneratorResponse) {
		this.datageneratorResponse = datageneratorResponse;
	}

	public Configuration getConfig() {
		return config;
	}

	public void setConfig(Configuration config) {
		this.config = config;
	}
	
	public DataGeneratorClient(Configuration config, String json,String id) {
		setConfig(config);
		this.setDatageneratorResponse("null");
		this.dgip = config.getDatageneratorip();
		this.dgport = config.getDatageneratorport();
		String usecase = config.getUsecase();
		this.dginput = new dataGeneratorInputConfig();
		this.dginput.setUsecase(config.getUsecase());
		this.dginput.setTopologyJson(json);
		this.dginput.setNetId(id);
		this.initiateurl = "http://"+dgip+":"+dgport+"/initiateusecase";
		log.info("initiating usecase "+ usecase);
		this.kpiurl = "http://"+dgip+":"+dgport+"/getpmkpidata/";
		initiateusecase(config.getUsecase());
	}

	public String initiateusecase(String usecase) {
		ObjectMapper obj = new ObjectMapper();
		String jsonstringconfig="";
		String responseStr = "";
		try {
			//jsonstringconfig = obj.enable(SerializationFeature.WRAP_ROOT_VALUE).writerWithDefaultPrettyPrinter().writeValueAsString(dginput);
			jsonstringconfig = obj.writeValueAsString(dginput);
			HttpClient client = HttpClient.newHttpClient();

			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(initiateurl))
					.POST(HttpRequest.BodyPublishers.ofString(jsonstringconfig))
					.header("Content-Type","application/json")
					.build();
			HttpResponse<String> response =
					client.send(request, HttpResponse.BodyHandlers.ofString());
			responseStr = response.body().toString();
			System.out.println(responseStr);
			log.info("Initiaeusecase response ---> "+responseStr);
			//HashMap<String,String> jobj = gson.fromJson(responsebody, HashMap.class);
			//setNetworkId(jobj.get("Message"));
			
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred while connecting to Data Generator"+ e.getLocalizedMessage());
		log.debug("Exception occurred while connecting to Data Generator"+ e.getLocalizedMessage());
		}
		return responseStr;		
	}
	public String callKpiApi(String id) {
		String responsestr = "";
		try {
			
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(this.kpiurl+id))
					.build();

			HttpResponse<String> response =
					client.send(request, BodyHandlers.ofString());
			responsestr = response.body().toString();
			setDatageneratorResponse(response.body());
			//System.out.println(response.body());
			log.info("callKpiApi response ---> "+responsestr);

		} catch (IOException | InterruptedException e) {
			//e.printStackTrace();
			System.out.println("Exception occurred while fetching data from Data Generator"+ e.getLocalizedMessage());
			log.debug("Exception occurred while fetching data from Data Generatorr"+ e.getLocalizedMessage());

		}
		return responsestr;
	}
	

	public String startDataGeneration(int rop, String starttime, String stoptime, int srop ) {
		String responsestr = "";
		ObjectMapper obj = new ObjectMapper();
		String jsonstringconfig="";
		this.dginput.setRop(rop);
		this.dginput.setStarttime(starttime);
		this.dginput.setStoptime(stoptime);
		this.dginput.setSrop(srop);
		try {
			
			String starturl =  "http://"+dgip+":"+dgport+"/startdatageneration";
			jsonstringconfig = obj.writeValueAsString(this.dginput);
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(starturl))
					.POST(HttpRequest.BodyPublishers.ofString(jsonstringconfig))
					.header("Content-Type","application/json")
					.build();

			HttpResponse<String> response =
					client.send(request, BodyHandlers.ofString());
			responsestr = response.body().toString();
			log.info("startDataGeneration response ---> "+responsestr);

			
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred while starting Data Generator"+ e.getLocalizedMessage());
			log.debug("Exception occurred while fetching starting Data Generatorr"+ e.getLocalizedMessage());

		}
		return responsestr;
	}
	
	
	public String triggerCmUpdate(String newtopo) {
		String resp = "";
		try {
			String notifyurl =  "http://"+dgip+":"+dgport+"/triggerconfigchange/";
			HttpClient client = HttpClient.newHttpClient();
			//HttpRequest request = HttpRequest.newBuilder()
			//		.uri(URI.create(notifyurl))
			//		.build();
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(notifyurl+this.dginput.getNetId()))
					.POST(HttpRequest.BodyPublishers.ofString(newtopo))
					.header("Content-Type","application/json")
					.build();

			HttpResponse<String> response =
					client.send(request, BodyHandlers.ofString());
			resp = response.body().toString();
			log.info("triggerCmUpdate response ---> "+resp);

			
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred while notifying Data Generator"+ e.getLocalizedMessage());
			log.debug("Exception occurred while notifying Data Generator"+ e.getLocalizedMessage());

		}
		return resp;
	}
	
	public String stopDataGeneration(String id) {
		String resp = "";
		try {
			String starturl =  "http://"+dgip+":"+dgport+"/stopdatageneration/";
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(starturl+id))
					.build();

			HttpResponse<String> response =
					client.send(request, BodyHandlers.ofString());
			resp =  response.body().toString();		
			
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred while Stopping Data Generator"+ e.getLocalizedMessage());
			log.debug("Exception occurred while Stopping Data Generator"+ e.getLocalizedMessage());

		}
		return resp;
	}
	
	public String deleteDataGeneration(String id) {
		String resp="";
		try {
			String starturl =  "http://"+dgip+":"+dgport+"/deletedatagenerator/";
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(starturl+id))
					.build();

			HttpResponse<String> response =
					client.send(request, BodyHandlers.ofString());
			resp =  response.body().toString();		
			
		} catch (IOException | InterruptedException e) {
			log.debug("Exception occurred while Deleting Data Generator thread"+ e.getLocalizedMessage());

		}
		return resp;
	}
	

}
