package ndp.controller;

public class StartRequestModel {
	
	private String starttime;
	private String stoptime;
	private int rop;
	private String networkId;
	private int srop;
	
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getStoptime() {
		return stoptime;
	}
	public void setStoptime(String stoptime) {
		this.stoptime = stoptime;
	}
	
	public StartRequestModel() {
		System.out.println("Start Request Model Constructor Called");
	}
	public String getNetid() {
		return networkId;
	}
	public void setNetid(String netid) {
		this.networkId = netid;
	}
	public int getRop() {
		return rop;
	}
	public void setRop(int rop) {
		this.rop = rop;
	}
	public int getSrop() {
		return srop;
	}
	public void setSrop(int srop) {
		this.srop = srop;
	}
}
