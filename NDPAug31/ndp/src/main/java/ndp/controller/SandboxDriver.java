package ndp.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ndp.networkAdapter.ConfigChangeNotification;


public class SandboxDriver {

	private static final Logger log = Logger.getLogger(SandboxDriver.class);

	private static HashMap<String,SandboxWorker> threadObjMap = new HashMap<String,SandboxWorker>();
	private static HashMap<SandboxWorker,Configuration> configObjMap = new HashMap<SandboxWorker,Configuration>();
	private static String networkadapterip;
	private static String networkadapterport;
//	private static String o1urlpath;
	private static String topoapipath;
	private static String pmapipath;
	private static String cmackapipath;
	
	
	public static String getTopoapipath() {
		return topoapipath;
	}

	public static void setTopoapipath(String topoapipath) {
		SandboxDriver.topoapipath = topoapipath;
	}

	public static String getPmapipath() {
		return pmapipath;
	}

	public static void setPmapipath(String pmapipath) {
		SandboxDriver.pmapipath = pmapipath;
	}

	public static String getCmackapipath() {
		return cmackapipath;
	}

	public static void setCmackapipath(String cmackapipath) {
		SandboxDriver.cmackapipath = cmackapipath;
	}
	
	public static String getnetworkadapterip() {
		return networkadapterip;
	}

	public static void setnetworkadapterip(String networkadapterip) {
		SandboxDriver.networkadapterip = networkadapterip;
	}

	public static String getnetworkadapterport() {
		return networkadapterport;
	}

	public static void setnetworkadapterport(String networkadapterport) {
		SandboxDriver.networkadapterport = networkadapterport;
	}

	public static String startThread(StartRequestModel model) {
		System.out.println("Start thread called--"+model.getRop());
		log.info("NDP start Data Generation called");
		try {
		if(model.getNetid().equals(null)||model.getNetid().equals("")||
				model.getStarttime().equals(null)||model.getStarttime().equals("")||
				model.getStoptime().equals(null)||model.getStoptime().equals("")||
				model.getRop()==0||
				model.getSrop()==0) {
			return "Please provide valid parameters";
		}else {
		SandboxWorker worker = threadObjMap.get(model.getNetid());
		worker.setRop(model.getRop());
		worker.setStarttime(model.getStarttime());
		worker.setStoptime(model.getStoptime());
		worker.setLogtime(String.valueOf(model.getStarttime()));
		worker.setSrop(model.getSrop());
		worker.start();
		return "Thread id "+ worker.getThreadId() + " started!!!";
		}
		}catch(Exception e) {
			System.out.println("Exception occurred in starting the thread " + e.getLocalizedMessage());
			return "Please provide valid parameters";
		}
	}
	
	public  static String stopThread(String id) {
		log.info("NDP stop Data Generation called");
		if(threadObjMap.containsKey(id)) {
		SandboxWorker worker = threadObjMap.get(id);
		worker.stop(id);
		return "Thread id "+ worker.getThreadId() + " stopped!!!";
		} else {
			return "Network Id doesnot exist!!!";
		}
	}
	
/* if CM comes direct from rapp.
	public  static String sendNextRopData(String id) {
		log.info("NDP send data called");
		if(threadObjMap.containsKey(id)) {
		SandboxWorker worker = threadObjMap.get(id);
		worker.sendnextpmdata();
		return "Thread id "+ worker.getThreadId() + " stopped!!!";
		} else {
			return "Network Id doesnot exist!!!";
		}
	}
	*/
	public static String addConfiguration(Configuration conf) {
		log.info("NDP Add configurations called");
		String jsonstring = "";
		boolean present = false;
		HashMap<String,Configuration> respmap = new HashMap<String,Configuration>();
		for(Configuration configurationitr : configObjMap.values()) {
			System.out.println(configurationitr );
			System.out.println(conf);
			if(configurationitr.equals(conf) || configurationitr==conf) {
				System.out.println("Configuration exists already !!!!");
				log.info("Configuration exists already !!!!");
				jsonstring = "Configuration exists already !!!!";
				present = true;
				//return "Configuration exists already !!!!";
			}
		}
		if(!present) {
		Configuration config = new Configuration();
		config = conf;		
		
		SandboxWorker sbworker = new SandboxWorker(config);
		threadObjMap.put(sbworker.getThreadId(), sbworker);
		configObjMap.put(sbworker, config);	
		respmap.put(sbworker.getNetid(), config);
		
		ObjectMapper obj = new ObjectMapper();
		try {
			jsonstring = obj.enable(SerializationFeature.WRAP_ROOT_VALUE).writerWithDefaultPrettyPrinter().writeValueAsString(respmap);
			System.out.println("add config"+ jsonstring);

		} catch (JsonProcessingException e) {
			jsonstring = "Exception occurred while adding configuration"+ e.getLocalizedMessage();
		log.debug("Exception occurred while adding configurations" + e.getLocalizedMessage());
		}
		}
		return jsonstring;
		
	}
	
	public  static void processPmData(String id, String data) {
		SandboxWorker worker = threadObjMap.get(id);
		worker.setPmjson(data);
	}
	
	public static String getConfiguration() {
		ArrayList<Object> conflist = new ArrayList<Object>();
		for(SandboxWorker sbworker: configObjMap.keySet()) {
			HashMap<String,Configuration> temp = new HashMap<String,Configuration>();
			temp.put(sbworker.getNetid(), sbworker.getConfig());
			conflist.add(temp);
		}
		String jsonstring = "";
		ObjectMapper obj = new ObjectMapper();
		try {
			jsonstring = obj.enable(SerializationFeature.WRAP_ROOT_VALUE).writerWithDefaultPrettyPrinter().writeValueAsString(conflist);
			System.out.println("get config"+ jsonstring);

		} catch (JsonProcessingException e) {
			jsonstring = "Exception occurred while fetching configuration"+ e.getLocalizedMessage();
		}
		return jsonstring;	
	}
	
	public static String deleteConfiguration(String netid) {
		log.info("NDP Delete configurations called" + netid);
		String response = "";
		if(threadObjMap.containsKey(netid)) {
		SandboxWorker sbworker = threadObjMap.get(netid);
		sbworker.delete(netid);
		configObjMap.remove(sbworker);
		threadObjMap.remove(netid);
		response = "Configuration deleted successfully";
		}
		else {
			response = "Configuration doesnot exist";
		}
		
		return response;	
	}
	
	public static String configupdate(ConfigChangeNotification cmbody) {
		log.info("NDP Config update called");
		String response = "";
		String netid = cmbody.getNetworkId();
		if(threadObjMap.containsKey(netid)) {
		SandboxWorker sbworker = threadObjMap.get(netid);
		sbworker.updateTopologyJson(cmbody);
		sbworker.setConfigChangeFlag(true);
		response = "Configuration received successfully";
		}
		else {
			response = "Configuration doesnot exist";
		}
		
		return response;	
	}
	
	public SandboxDriver() {
		System.out.println("Sandbox Driver Called");
		log.info("NDP driver running");

		NDPController controller = new NDPController();
	/*	StartDataGeneratorController dgcontroller = new StartDataGeneratorController();
		Thread dgcontrollerthread = new Thread(dgcontroller);
		dgcontrollerthread.start();
		*/
		//dgcontroller.execfile("../datagenerator/SandboxNdp.py");
		
	}
	
	

	public static void main(String[] args) throws IOException {
		System.out.println("Sandbox Drive Running!!!!");
		SandboxDriver driver = new SandboxDriver();
		driver.parseConfig(args[0]);
	}
	
	
//InputStream inputStream = null;
	
	private void parseConfig(String arg) throws IOException {
		 
		try {
			Properties prop = new Properties();
			//String propFileName = "./config.properties";
			//inputStream = new FileInputStream(propFileName);			
			//inputStream = new FileInputStream(System.getProperty("config.properties"));			
			FileReader inputStream = new FileReader(arg);
			//prop.load(in);
			
			if (inputStream != null) {
				prop.load(inputStream);
			}
			String ip = prop.getProperty("networkadapterip");
			//setO1agentip(ip);
			setnetworkadapterip(ip);
			
			String port = prop.getProperty("networkadapterport");
			//setO1agentport(port);;
			setnetworkadapterport(port);
			
			String topopath = prop.getProperty("toponotifpath");
			setTopoapipath(topopath);
			String pmpath = prop.getProperty("pmnotifpath");
			setPmapipath(pmpath);
			String cmpath = prop.getProperty("cmnotifpath");
			setCmackapipath(cmpath);
			
			inputStream.close();
			} catch (Exception e) {
			System.out.println("Exception occurred while setting network adapter properties " + e);
			log.debug("Exception occurred while setting network adapter properties " + e.getLocalizedMessage());
		} 
	}

	
/*
	public static String getO1urlpath() {
		return o1urlpath;
	}

	public static void setO1urlpath(String o1urlpath) {
		SandboxDriver.o1urlpath = o1urlpath;
	}
	*/
	
}

////////////////////////////////////////////////////////////				
		
		
	/*	AneDriver ie = new AneDriver();
		 
	      ie.execfile("/home/ubuntu/eclipse-workspace/SandboxIntegrationv1/src/Pythonfiles/src_ane/Anre.py");
	     // ie.execfile("/home/ubuntu/eclipse-workspace/SandboxIntegrationv1/src/Pythonfiles/hello.py");


	      PyInstance Anre = ie.createClass("Anre", "None");
	 
	      Anre.invoke("parseCsv");
	    */
		//String[] cmd = new String[]{"/bin/sh", "/home/ubuntu/eclipse-workspace/Sandbox/SandboxNdp.py"};
/////////////////////////////////////////////////////////////////////////////
	/*	GenerateTopology genTopo = new GenerateTopology(config);
		
		ObjectMapper obj = new ObjectMapper();
		obj.setSerializationInclusion(Include.NON_NULL);
		try {
			String jsonstringTopo = obj.enable(SerializationFeature.WRAP_ROOT_VALUE).writerWithDefaultPrettyPrinter().writeValueAsString(genTopo.getTopo());
			TopologyRequestHandler.setTopology(jsonstringTopo);
			
			String jsonstringConfig = obj.enable(SerializationFeature.WRAP_ROOT_VALUE).writerWithDefaultPrettyPrinter().writeValueAsString(genTopo.getCellsconfig());
			ConfigRequestHandler.setCellConfig(jsonstringConfig);

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	//	System.out.println(gson.toJson(genTopo.getTopo()));
	//	System.out.println(gson.toJson(genTopo.getCellsconfig()));
	//	TopologyRequestHandler.setTopology(gson.toJson(genTopo.getTopo()));
	//	ConfigRequestHandler.setCellConfig(gson.toJson(genTopo.getCellsconfig()));
//	}
	

//}
