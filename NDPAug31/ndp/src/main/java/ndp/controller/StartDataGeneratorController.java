
package ndp.controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import org.python.core.PyInstance;
import org.python.util.PythonInterpreter;



public class StartDataGeneratorController implements Runnable{
	InputStream inputStream = null;
	private static String path="";
	public StartDataGeneratorController() {
		System.out.println("Starting Data Generator");
		try {
			path = parseConfig();
		} catch (IOException e) {
			System.out.println("Exception in Starting Data Generator");
		}
	}
	
	@Override
	public void run() {
		try {
			Process pr = Runtime.getRuntime().exec("python3 "+path);
			BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));			 
            String line=null;
            while((line=input.readLine()) != null) {
                System.out.println(line);
            } 
		} catch (IOException e) {
            System.out.println("Exception while fetching output from data generator"+ e.getLocalizedMessage());

		}
		System.out.println("Data Generator Running");
	}
	
	private String parseConfig() throws IOException {

		try {
			Properties prop = new Properties();
			//String propFileName = "config/config.properties";
			//inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);	
			String propFileName = "./config.properties";
			inputStream = new FileInputStream(propFileName);			

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			String datageneratorlocation = prop.getProperty("datageneratorpath");
			return datageneratorlocation;
			} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return "NA";
	}
}




/*
public class StartDataGeneratorController {
	PythonInterpreter interpreter = null;

	public StartDataGeneratorController() {
		PythonInterpreter.initialize(System.getProperties(),System.getProperties(), new String[0]);
		this.interpreter = new PythonInterpreter();
	}
	
	public void execfile( final String fileName )
	   {
	      this.interpreter.execfile(fileName);
	   }
	 
	   public PyInstance createClass( final String className, final String opts )
	   {
	      return (PyInstance) this.interpreter.eval(className + "(" + opts + ")");
	   }
	   
	   
}
*/
/*
public class StartDataGeneratorController implements Runnable{
	InputStream inputStream = null;
	private static String path="";
	  public StartDataGeneratorController() {
			System.out.println("Starting Python Simulator");
			try {
				path = parseConfig();
			} catch (IOException e) {
				System.out.println("Exception in Starting Python Simulator");
			}
		}
	  @Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("Starting Python Thread");
			String[] cmd = new String[]{"/bin/sh", path};
			try {
				Process pr = Runtime.getRuntime().exec(cmd);
				pr.getInputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	  private String parseConfig() throws IOException {

			try {
				Properties prop = new Properties();
				//String propFileName = "config/config.properties";
				//inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);	
				String propFileName = "./config.properties";
				inputStream = new FileInputStream(propFileName);			

				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}

				String datageneratorlocation = prop.getProperty("shellscriptpath");
				return datageneratorlocation;
				} catch (Exception e) {
				System.out.println("Exception: " + e);
			} finally {
				inputStream.close();
			}
			return "NA";
		}
  }
 */
