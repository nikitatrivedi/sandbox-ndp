package ndp.controller;

import java.util.ArrayList;

public class EsPmDataModel {

	
	private String messageType;
	private String networkType;
	private String networkId;
	private String nodeType;
	private String iocType;
	private String useCase;
	private String sequence;
	private ArrayList<Object> data;
	
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getIocType() {
		return iocType;
	}

	public void setIocType(String iocType) {
		this.iocType = iocType;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public ArrayList<Object> getData() {
		return data;
	}

	public void setData(ArrayList<Object> data) {
		this.data = data;
	}

	
	public EsPmDataModel() {
		this.data = new ArrayList<Object>();
		
	}
	
}
