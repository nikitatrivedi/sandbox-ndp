package ndp.controller;

import java.text.ParseException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;

import ndp.esdb.EsdbClient;
import ndp.networkAdapter.ConfigChangeNotification;
import ndp.networkAdapter.ConfigData;
import ndp.networkAdapter.Notification;
import ndp.networkAdapter.NetworkAdapterClient;
import ndp.requestHandler.NotificationRequestHandler;
import ndp.topology.RanAuto.InputFileReaderRanAuto;
import ndp.topology.es.InputFileReaderES;

public class SandboxWorker implements Runnable{
	private static final Logger log = Logger.getLogger(SandboxWorker.class);

	private  String dbip="127.0.0.1";
	private  String dbport="9200";
	private  String dgip="127.0.0.1";
	private  String dgport="5000";
	
	
	private static Gson gson = new Gson();
	private String ThreadId;
	
	private String Stoptime;
	private String starttime;
	
	private String logtime;
//	private String topotime;
	
	private int rop;
	private int srop;
	
	private String usecase;
	private String netid;
	//private ArrayList<String> pmdatalist; //direct from rapp

	private String topologyJson="";
	private HashMap<String,String> netIdTopoMap = new HashMap<String,String>();
	private DataGeneratorClient dgclient;
	private EsdbClient esdbclient ;
	private InputFileReaderES filereaderES;
	private InputFileReaderRanAuto filereaderRanAuto;	
	private boolean configChangeFlag = false;
	private int lastIndex = 0;
	private NetworkAdapterClient netadapclient;	
	private boolean worker = true;
	private boolean dgflag = false;
	private Configuration config;
	private Thread sbthread;
	private String nettype;
	private String pmjson = "";
	private int numrops = 0;

	public  String getNetid() {
		return this.netid;
	}

	public  void setNetid(String netid) {
		this.netid = netid;
	}
	
	public String getStarttime() {
		return this.starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	
	public String getLogtime() {
		return this.logtime;
	}
	public void setLogtime(String logtime) {
		this.logtime = logtime;
	}
	
	public String getStoptime() {
		return this.Stoptime;
	}
	public void setStoptime(String stoptime) {
		this.Stoptime = stoptime;
	}
	public int getRop() {
		return this.rop;
	}
	public void setRop(int rop) {
		this.rop = rop;
	}
	
	public  String getDbip() {
		return this.dbip;
	}

	public  void setDbip(String dbip) {
		this.dbip = dbip;
	}

	public  String getDbport() {
		return this.dbport;
	}

	public  void setDbport(String dbport) {
		this.dbport = dbport;
	}

	public  String getDgip() {
		return this.dgip;
	}

	public  void setDgip(String dgip) {
		this.dgip = dgip;
	}

	public String getDgport() {
		return this.dgport;
	}

	public void setDgport(String dgport) {
		this.dgport = dgport;
	}
	
	
	public SandboxWorker(Configuration config) {
		System.out.println("Sandbox Worker constructor");
		setUsecase(config.getUsecase());
		setConfig(config);
		setDbip(config.getDbip());
		setDbport(config.getDbport());
		setDgip(config.getDatageneratorip());
		setDgport(config.getDatageneratorport());
		
		//this.topotime = java.time.LocalDateTime.now().toInstant(ZoneOffset.UTC).toString();
//		this.topotime = config.getTopotimestamp();
//		this.logtime = topotime;
				
		this.esdbclient = new EsdbClient(config.getDbip(),config.getDbport());
		String topo = generateTopologyJson(config);
		int timeout = 30;
		this.netadapclient = new NetworkAdapterClient(SandboxDriver.getnetworkadapterip(),SandboxDriver.getnetworkadapterport());

		
		String notif = storeTopology();
		if(!notif.contentEquals("")) {
			sendTopoNotification(notif,timeout);
		}
		this.sbthread = new Thread(this, netid);
		
		System.out.println("Thread Name is ---> " + sbthread.getName() + " " +this.dbip);
		log.info("Thread Name is ---> " + sbthread.getName() + " " +this.dbip);

		setThreadId(this.sbthread.getName());
		this.dgclient = new DataGeneratorClient(config,topologyJson,this.netid);

		
	}
	
	/*
	 * if cm comes directly from rApp.
	private static int sequence = 0;
	public void sendnextpmdata() {
		String pmindex  = "pmindex_"+getUsecase().toLowerCase()+"_"+this.netid;
		sequence++;
		int doccount=this.lastIndex;
		int start = doccount;
		boolean status = false;
		@SuppressWarnings("unchecked")
		ArrayList<String> list = gson.fromJson(pmdatalist.get(0), ArrayList.class);
		if(list.size()>0) {	
			for(int docint = 1; docint<= list.size(); docint++) {
				System.out.println(list.get(docint-1));
				status = this.esdbclient.putData(pmindex, String.valueOf(doccount), list.get(docint-1));					
				doccount++;
				log.info("Thread"+this.ThreadId + "PM:"+list.get(docint-1));
			}
		
			int end = doccount-1;
			String msg = "Process PM Data"+ status;					
			String time = Instant.now().toString();
			Notification notification = new Notification("PM", this.netid, pmindex, String.valueOf(start), String.valueOf(end), msg,time);
			System.out.println(gson.toJson(notification));
			System.out.println("startIndex " + start + " endIndex" + end);
			this.lastIndex = end;
			log.info("Thread"+this.ThreadId +"Notification:"+gson.toJson(notification));
			if(status) {
			this.netadapclient.sendNotification(gson.toJson(notification), SandboxDriver.getPmapipath()+this.nettype);							
				}
			}
		pmdatalist.remove(0);
	}
	*/
	@Override
	public void run() {
		String pmindex  = "pmindex_"+getUsecase().toLowerCase()+"_"+this.netid;
		long currentTime=System.currentTimeMillis();
		long iterTime = System.currentTimeMillis();
		
		String currentTimetemp = getStarttime();
		int doccount=this.lastIndex;
		while(this.worker) {
			
			if(isConfigChangeFlag()) {
			//	this.dgclient.triggerCmUpdate(this.topologyJson);				
				setConfigChangeFlag(false);
				
			}
			
			if(!getPmjson().equals("")) {
				//pmdatalist.add(getPmjson());	// direct from rapp
				currentTimetemp=Instant.parse(currentTimetemp).plusSeconds(this.rop).toString();
				System.out.println("log time-----> " + logtime);
				System.out.println("getPm-->" + getPmjson());
				int start = doccount;
				boolean status = false;
				@SuppressWarnings("unchecked")
				ArrayList<String> list = gson.fromJson(getPmjson(), ArrayList.class);
				if(list.size()>0) {	
					for(int docint = 1; docint<= list.size(); docint++) {
						System.out.println(list.get(docint-1));
						status = this.esdbclient.putData(pmindex, String.valueOf(doccount), list.get(docint-1));					
						doccount++;
						log.info("Thread"+this.ThreadId + "PM:"+list.get(docint-1));
						

					}
					setPmjson("");
					int end = doccount-1;
					String msg = "Process PM Data"+ status;			
						
					String time = Instant.now().toString();
					Notification notification = new Notification("PM", this.netid, pmindex, String.valueOf(start), String.valueOf(end), msg,time);
					System.out.println(gson.toJson(notification));
					System.out.println("startIndex " + start + " endIndex" + end);
					this.lastIndex = end;
					log.info("Thread"+this.ThreadId +"Notification:"+gson.toJson(notification));

					if(status) {
					this.netadapclient.sendNotification(gson.toJson(notification), SandboxDriver.getPmapipath()+this.nettype,getSrop());							
						}
					}				
				iterTime = System.currentTimeMillis();
				this.logtime =Instant.parse(this.logtime).plusSeconds(this.rop).toString();
			//	this.logtime =Instant.parse(this.logtime).plusSeconds(this.srop).toString();
			}
			/*
		//	if(((currentTime-iterTime)/1000 >= this.rop)&& this.dgflag) {	
			if(((currentTime-iterTime)/1000 >= this.srop)&& this.dgflag) {	
				System.out.println("log time-----> " + logtime);
				this.dgclient.callKpiApi(this.netid);
				String response = this.dgclient.getDatageneratorResponse();
				@SuppressWarnings("unchecked")
				ArrayList<String> list = gson.fromJson(response, ArrayList.class);
				int start = doccount;
				boolean status = false;
				if(list.size()>0) {	
				for(int docint = 1; docint<= list.size(); docint++) {
					System.out.println(list.get(docint-1));
					status = this.esdbclient.putData(pmindex, String.valueOf(doccount), list.get(docint-1));					
					doccount++;
					log.info("Thread"+this.ThreadId + "PM:"+list.get(docint-1));

				}
				int end = doccount-1;
				String msg = "Process PM Data"+ status;			
					
				String time = Instant.now().toString();
				//String time = logtime;				
				Notification notification = new Notification("PM", this.netid, pmindex, String.valueOf(start), String.valueOf(end), msg,time);
				System.out.println(gson.toJson(notification));
				System.out.println("startIndex " + start + " endIndex" + end);
				this.lastIndex = end;
				log.info("Thread"+this.ThreadId +"Notification:"+gson.toJson(notification));

			//	if(status) {
				this.netadapclient.sendNotification(gson.toJson(notification), SandboxDriver.getPmapipath()+this.nettype);			
			//	}
				}				
			iterTime = System.currentTimeMillis();
			this.logtime =Instant.parse(this.logtime).plusSeconds(this.rop).toString();
		//	this.logtime =Instant.parse(this.logtime).plusSeconds(this.srop).toString();
			}
			*/
			
			
			currentTime=System.currentTimeMillis();
		/*	if(Instant.parse(this.logtime).isAfter(Instant.parse(getStoptime()))) {
				System.out.println("Stopping Sandbox Driver !!!!"+ (Instant.parse(this.logtime).compareTo(Instant.parse(getStoptime()))));
				stop(this.netid);
				
			}
			*/
			if(Instant.parse(currentTimetemp).isAfter(Instant.parse(getStoptime()))) {
				System.out.println("Stopping Sandbox Driver !!!!"+ (Instant.parse(currentTimetemp).compareTo(Instant.parse(getStoptime()))));
				stop(this.netid);			
			}
		}
		
	}
	public void stop(String id) {
		this.worker = false;
		this.dgclient.stopDataGeneration(id);
		System.out.println("Stopping Thread");
		log.info("Stopping Thread");
		
	}
	
	public void delete(String id) {
		this.worker = false;
		this.dgclient.stopDataGeneration(id);
		this.dgclient.deleteDataGeneration(id);
		System.out.println("Deleting Thread");
		log.info("Deleting Thread");

		
	}
	public  void start() {		
		if(!this.worker) {
			System.out.println("restarting thread");
			log.info("restarting thread");

			this.sbthread = new Thread(this, netid);
			this.sbthread.start();
			this.worker = true;
			
		}else {
			System.out.println("starting thread initially");
			log.info("starting thread initially");

			this.sbthread.start();
			}
	//	startDataGenerator(getRop(),getLogtime(), getStoptime(), getSrop());
		startDataGenerator(getRop(),getStarttime(), getStoptime(), getSrop());

	}
	
	public String getUsecase() {
		return usecase;
	}
	public void setUsecase(String usecase) {
		this.usecase = usecase;
	}
	
	public void startDataGenerator(int rop, String time, String stoptime, int srop) {
		String response = this.dgclient.startDataGeneration(rop,time, stoptime,srop );
		this.dgflag = true;
		log.info("Thread"+this.ThreadId +"Sandbox DG started!!!");
		System.out.println("Data Generator Start response---> " + response);
	}

	
	public String generateTopologyJson(Configuration config) {
		String uc = getUsecase();
	//	String time = this.topotime;
		String path = "" ;
		switch(uc) {
		case "RanAuto":
			this.filereaderRanAuto = new InputFileReaderRanAuto();
			this.topologyJson = this.filereaderRanAuto.generateRanAutoTopology(config.getFilepath());
			this.netid = this.filereaderRanAuto.getNetworkId();
			path = this.filereaderRanAuto.getFilePath();
			this.nettype = "lte";
			break;
		case "ES":
			this.filereaderES = new InputFileReaderES();
			this.topologyJson = this.filereaderES.createTopologyJson(config.getFilepath());
			this.netid = this.filereaderES.getNetworkId();
			path = this.filereaderES.getFilePath();
			this.nettype = "5g";
			break;
		case "AD":
			this.filereaderES = new InputFileReaderES();
			this.topologyJson = this.filereaderES.createTopologyJson(config.getFilepath());
			this.netid = this.filereaderES.getNetworkId();
			path = filereaderES.getFilePath();
			this.nettype = "5g";
			break;
		default:
			System.out.println("Undetermined usecase");
			log.info("Usecase not known");
			break;
		}
		netIdTopoMap.put(netid, topologyJson);
		return path;
	}
	
	public String storeTopology() {
		String topoindex = "topologyindex_"+getUsecase().toLowerCase()+"_"+this.netid;
		String docid = "1";
		String topo = this.topologyJson;
		String notifiString = "";
		//this.esdbclient.createIndex(topoindex);
		boolean status = this.esdbclient.putData(topoindex, docid, topo);
		String msg = "Create Topology Notification " + status;
		String time = Instant.now().toString();
		//String time = this.logtime;
		Notification notification = new Notification("TOPOLOGY", this.netid, topoindex, docid, docid, msg,time);
		System.out.println(gson.toJson(notification));
		if(status) {
			//this.netadapclient.sendNotification(gson.toJson(notification),SandboxDriver.getTopoapipath());
			notifiString = gson.toJson(notification);
		}
		log.info("Thread"+this.ThreadId +"Topology:"+ topo);
		return notifiString;
	}
	
	public void sendTopoNotification(String notif,int timeout) {
		this.netadapclient.sendNotification(notif,SandboxDriver.getTopoapipath()+this.nettype,timeout);
		log.info("Thread"+this.ThreadId +"Notification:"+notif);
	}
	public void updateTopologyJson(ConfigChangeNotification cmnotification) {
		//String confignotif = NotificationRequestHandler.getNotification();
		System.out.println("Inside Worker " + cmnotification);
		//ConfigChangeNotification cmnotification = gson.fromJson(confignotif, ConfigChangeNotification.class);
	//	System.out.println(cmnotification.getNetworkId()+"  " +cmnotification.getData().get(0));
		//configchangeStringfromO1 = confignotif;
		ArrayList<ConfigData> data = cmnotification.getData();
		ArrayList<ConfigData> Ackdata = new ArrayList<ConfigData>();
		boolean cellstatus = false;
		for(int i = 0; i< data.size(); i++) {
			String cellId = data.get(i).getId();
			String nrtci = data.get(i).getNrtci();
			ArrayList<Object> attributes = data.get(i).getAttributes();
			for(int j = 0; j< attributes.size(); j++) {
				@SuppressWarnings("unchecked")
				LinkedTreeMap<String,String> configParam = (LinkedTreeMap<String, String>) attributes.get(j);
				for(String key:configParam.keySet()) {
					HashMap<String,Object> respmap = this.filereaderES.updateTopology(cellId, key, configParam.get(key));
				//	ConfigData confdata = this.filereaderES.updateTopology(cellId, key, configParam.get(key));
					ConfigData confdata = (ConfigData) respmap.get("cmdata");
					cellstatus = (Boolean) respmap.get("flag");
				//	confdata.setNrtci(nrtci);
					Ackdata.add(confdata);
				}
			}
		}
		// Create Cm Ack Notification
		
		ConfigChangeNotification cmackObj = new ConfigChangeNotification();
		cmackObj.setData(Ackdata);
		cmackObj.setDeviceType(cmnotification.getDeviceType());
		cmackObj.setIocType(cmnotification.getIocType());
		cmackObj.setNetworkId(cmnotification.getNetworkId());
		cmackObj.setNetworkType(cmnotification.getNetworkType());
		cmackObj.setTimestamp(String.valueOf(Instant.now().toString()));
		//cmackObj.setTimestamp(this.logtime);
		cmackObj.setType("CONFIG_CHANGE");
		
		if(!cellstatus) {
			//cmackObj.setMessage("CellId does not found!!!");
			System.out.println("Cell Id does not found ");
			log.info("Cell Id does not found ");
		}else 
			{
			//cmackObj.setMessage("Config updated successfully");
			this.topologyJson = filereaderES.getTopojson();
			//this.dgclient.triggerCmUpdate(this.topologyJson);	
			this.dgclient.triggerCmUpdate(gson.toJson(cmackObj));

			//String notif = storeTopology();
			
			}
		this.netadapclient.sendNotification(gson.toJson(cmackObj),SandboxDriver.getCmackapipath()+this.nettype,getSrop());
		log.info("Thread"+this.ThreadId+"Notification Received:"+cmnotification);
		
		System.out.println("Config Ack Json--> " + gson.toJson(cmackObj));
		log.info("Notification:"+gson.toJson(cmackObj));
		
	}
	
	private String modifyDateLayout(String inputDate) throws ParseException{
		   Instant timestamp = Instant.parse(inputDate);
		   return timestamp.toString();
	}
	
	public  boolean isConfigChangeFlag() {
		return this.configChangeFlag;
	}
	public  void setConfigChangeFlag(boolean configChangeFlag) {
		this.configChangeFlag = configChangeFlag;
	}
	public Configuration getConfig() {
		return config;
	}
	public void setConfig(Configuration config) {
		this.config = config;
	}
	public String getThreadId() {
		return ThreadId;
	}
	public void setThreadId(String threadId) {
		ThreadId = threadId;
	}

	public int getSrop() {
		return srop;
	}

	public void setSrop(int srop) {
		this.srop = srop;
	}

	public String getPmjson() {
		return pmjson;
	}

	public void setPmjson(String pmjson) {
		this.pmjson = pmjson;
	}
}
