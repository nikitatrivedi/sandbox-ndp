package ndp.controller;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpServer;

import ndp.requestHandler.ConfigurationRequestHandler;
import ndp.requestHandler.DataGenerationRequestHandler;
import ndp.requestHandler.NotificationRequestHandler;
import ndp.requestHandler.EsPmDataRequestHandler;

public class NDPController {
	private static final Logger log = Logger.getLogger(NDPController.class);

	public NDPController() {
		HttpServer server;
		try {
			server = HttpServer.create(new InetSocketAddress("0.0.0.0", 7000), 0);
	    	server.createContext("/ndp/v1/post/notification", new NotificationRequestHandler());
	    	
	    	server.createContext("/ndp/v1/add/configuration", new ConfigurationRequestHandler());
	    	server.createContext("/ndp/v1/get/configuration", new ConfigurationRequestHandler());
	    	server.createContext("/ndp/v1/delete/configuration", new ConfigurationRequestHandler());

	    	server.createContext("/ndp/v1/startdatageneration", new DataGenerationRequestHandler());
	    	server.createContext("/ndp/v1/stopdatageneration", new DataGenerationRequestHandler());
	    	
	    	server.createContext("/ndp/v1/post/pmdata", new EsPmDataRequestHandler());

	    	
	    	//server.setExecutor(null);
	    	server.setExecutor(Executors.newFixedThreadPool(20));
	    	server.start();
	    	System.out.println(" NDP Server started on port 7000");
	    	log.info(" NDP Server started on port 7000");

		} catch (IOException e) {
			System.out.println("Exception occurred while starting NDP Controller " + e.getLocalizedMessage());
			log.debug("Exception occurred while starting NDP Controller " + e.getLocalizedMessage());

		}
	}
}
