package ndp.networkAdapter;

import java.util.UUID;
import java.time.*;
import java.util.Date;

public class Notification {
	//private UUID id;
	private String type; // pm data, topology
	private String  networkId;	
	private String timestamp;
	private String dbIndex; // esdb index
	private String docIdStart;
	private String docIdEnd;
	private String message;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}  
	
	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getDbIndex() {
		return dbIndex;
	}

	public void setDbIndex(String dbIndex) {
		this.dbIndex = dbIndex;
	}

	public String getDocIdStart() {
		return docIdStart;
	}

	public void setDocIdStart(String docIdStart) {
		this.docIdStart = docIdStart;
	}

	public String getDocIdEnd() {
		return docIdEnd;
	}

	public void setDocIdEnd(String docIdEnd) {
		this.docIdEnd = docIdEnd;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	public Notification(String type, String netId, String dbindex, String docstart, String docEnd, String msg, String time) {
		//this.id =UUID.randomUUID();
		//this.time = System.currentTimeMillis();
		//this.timestamp = Instant.now().toString();
		this.timestamp = time;
		this.type =  type;
		this.networkId = netId;
		this.dbIndex = dbindex;
		this.docIdStart = docstart;
		this.docIdEnd = docEnd;
		this.message = msg;
	}
	
	 @Override    
	    public String toString() {    
	        // TODO Auto-generated method stub    
	        return "\"Notification {type ="+ type + 
	        		", networkId = " + networkId +
	        		", timestamp = " + timestamp + 
	        		", dbIndex = "+ dbIndex +
	        		", docIdStart = "+ docIdStart +
	        		", docIdEnd = "+ docIdEnd +
	        		", message = "+ message +
	        		"} ";    
	    }
	
}
