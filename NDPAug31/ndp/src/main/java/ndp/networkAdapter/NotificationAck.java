package ndp.networkAdapter;

import java.util.UUID;
import java.time.*;
import java.util.Date;

public class NotificationAck {
	//private UUID id;
	private String type; // pm data, topology
	private String  networkId;	
	private String networkType;
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	private String message;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}  
	
	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	
	public NotificationAck() {	
	}
	
	 @Override    
	    public String toString() {    
	        // TODO Auto-generated method stub    
	        return "\"Notification {type ="+ type + 
	        		", networkType = " + networkType +
	        		", networkId = " + networkId +
	        		", status = " + status +       		
	        		", message = "+ message +
	        		"} ";    
	    }
	
}
