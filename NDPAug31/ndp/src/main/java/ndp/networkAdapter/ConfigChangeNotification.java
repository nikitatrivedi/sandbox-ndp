package ndp.networkAdapter;

import java.util.ArrayList;

public class ConfigChangeNotification {
	
	private String type; // pm data, topology
	private String networkType;
	private String  networkId;	
	private String timestamp;
	//private String message;	NWA doesnot support message field
	private String deviceType;
	private String iocType;
	private ArrayList<ConfigData> data;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
/*	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	*/
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getIocType() {
		return iocType;
	}
	public void setIocType(String iocType) {
		this.iocType = iocType;
	}
	public ArrayList<ConfigData> getData() {
		return data;
	}
	public void setData(ArrayList<ConfigData> data) {
		this.data = data;
	}
	
	public ConfigChangeNotification() {
		
	}
	
	
	@Override    
    public String toString() {    
        // TODO Auto-generated method stub    
        return "\"Notification {type ="+ type + 
        		", networkId = " + networkId +
        		", timestamp = " + timestamp + 
        		", deviceType = "+ deviceType +
        		", iocType = "+ iocType +
        		", data = "+ data +
        		//", message = "+ message +
        		"} ";    
    }
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	
}
