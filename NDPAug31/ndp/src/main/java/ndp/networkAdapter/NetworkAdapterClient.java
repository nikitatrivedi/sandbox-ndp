package ndp.networkAdapter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import ch.qos.logback.core.util.Duration;

public class NetworkAdapterClient {
	private static String o1agentip="";
	private static String o1agentport="";
	private static String o1url = "";//http://127.0.0.1:8080/postnotification";
	//private String o1url = " http://172.30.161.160:8085/shareddata/v1/notification";
	private String postnotifpath="";
	private String toponotifpath="";
	private String PMnotifpath="";
	private String CMnotifpath="";

	public String getToponotifpath() {
		return toponotifpath;
	}
	public void setToponotifpath(String toponotifpath) {
		this.toponotifpath = toponotifpath;
	}
	public String getPMnotifpath() {
		return PMnotifpath;
	}
	public void setPMnotifpath(String pMnotifpath) {
		PMnotifpath = pMnotifpath;
	}
	public String getCMnotifpath() {
		return CMnotifpath;
	}
	public void setCMnotifpath(String cMnotifpath) {
		CMnotifpath = cMnotifpath;
	}
	private String o1response="";
	
	public String getO1response() {
		return o1response;
	}
	public void setO1response(String o1response) {
		this.o1response = o1response;
	}
	
	
	public String getO1agentip() {
		return o1agentip;
	}
	public void setO1agentip(String o1agentip) {
		NetworkAdapterClient.o1agentip = o1agentip;
	}
	public String getO1agentport() {
		return o1agentport;
	}
	public void setO1agentport(String o1agentport) {
		NetworkAdapterClient.o1agentport = o1agentport;
	}
	
	public NetworkAdapterClient(String ip, String port) {
		setO1agentip(ip);
		setO1agentport(port);
		
		//setPostnotifpath(path);
	//	setToponotifpath(topopath);
	//	setPMnotifpath(pmpath);
	//	setCMnotifpath(cmpath);
			//parseConfig();
		
			o1url="http://"+getO1agentip()+":"+getO1agentport();//+getPostnotifpath();
			System.out.println("Network Adapter is at -->" + getO1agentip() + ":"+ getO1agentport());
	}
	
	public String getPostnotifpath() {
		return this.postnotifpath;
	}
	public void setPostnotifpath(String postnotifpath) {
		this.postnotifpath = postnotifpath;
	}
	public void sendNotification(String payload, String path, int timeout) {

		try {
			//String payload = new Notification(type).toString();
			
	                		
		/*	HttpClient client = HttpClient.newHttpClient();
		    HttpRequest request = HttpRequest.newBuilder()
		          .uri(URI.create(o1url))        
		          .build();
		          */
			System.out.println("Network Adapter path:" + o1url+path);
			HttpClient httpClient = HttpClient.newHttpClient();
			
			HttpRequest request = HttpRequest.newBuilder()
			          .uri(URI.create(o1url+path))
			          .POST(HttpRequest.BodyPublishers.ofString(payload))
			          .timeout(java.time.Duration.ofSeconds(timeout))
			          .build();
	        
		    HttpResponse<String> response =httpClient.send(request, BodyHandlers.ofString());
		    setO1response(response.body().toString());
		    System.out.println("Response code from network adapter--> " +response.statusCode());

		    System.out.println("Response from network adapter--> " +getO1response());
		    
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred while sending Notification to O1 Agent " + e.getLocalizedMessage());
		}
		
		
	}
	
}
