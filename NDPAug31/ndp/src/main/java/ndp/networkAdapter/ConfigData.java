package ndp.networkAdapter;

import java.util.ArrayList;

public class ConfigData {
	private String id;
	private String nrtci;
	private ArrayList<Object> attributes = new ArrayList<Object>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNrtci() {
		return nrtci;
	}
	public void setNrtci(String nrtci) {
		this.nrtci = nrtci;
	}
	public ArrayList<Object> getAttributes() {
		return attributes;
	}
	public void setAttributes(ArrayList<Object> attributes) {
		this.attributes = attributes;
	}
	
	public ConfigData() {
		this.id = null;
		this.nrtci = null;
		this.attributes =  new ArrayList<Object>();
		
	}
	
	@Override    
    public String toString() {    
        // TODO Auto-generated method stub    
        return "\" {id ="+ id + 
        		", nrtci = " + nrtci +
        		", attributes = " + attributes + 
        		"} ";    
    }
}
