package ndp.requestHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import ndp.controller.SandboxDriver;
import ndp.controller.SandboxWorker;
import ndp.controller.StartRequestModel;
import ndp.networkAdapter.ConfigChangeNotification;

public class DataGenerationRequestHandler implements HttpHandler {
	Gson gson = new Gson();

	
	@Override 
	public void handle(HttpExchange httpExchange) throws IOException {
		String requestParamValue=null;  
		if("GET".equals(httpExchange.getRequestMethod())) { 
			
			System.out.println(" stop dg request");
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
			InputStream is = httpExchange.getRequestBody();
			StringBuilder bodyStr = new StringBuilder();
			try (
	                InputStreamReader isr = new InputStreamReader(is,
	                        StandardCharsets.UTF_8);
	                BufferedReader br = new BufferedReader(isr)) {
	            br.lines().forEach(line -> bodyStr.append(line));	            
	        }	
			requestParamValue = handleThreadStopRequest(bodyStr.toString());   			
			//setNotification(bodyStr.toString());
			handleThreadStopResponse(httpExchange,requestParamValue); 		
			
		}else if("POST".equals(httpExchange.getRequestMethod())) { 
			
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
			InputStream is = httpExchange.getRequestBody();
			StringBuilder bodyStr = new StringBuilder();
			try (
	                InputStreamReader isr = new InputStreamReader(is,
	                        StandardCharsets.UTF_8);
	                BufferedReader br = new BufferedReader(isr)) {
	            br.lines().forEach(line -> bodyStr.append(line));	            
	        }		
			if((bodyStr.toString().contains("starttime"))) {
				System.out.println(" start dg request");

			requestParamValue = handleThreadStartRequest(bodyStr.toString());   			
			//setNotification(bodyStr.toString());
			handleThreadStartResponse(httpExchange,requestParamValue); 
			} else {
				System.out.println(" stop dg request");
				requestParamValue = handleThreadStopRequest(bodyStr.toString());   			
				//setNotification(bodyStr.toString());
				handleThreadStopResponse(httpExchange,requestParamValue); 
			}
		}  
	}



	private String handleThreadStartRequest(String reqbody) {
		if(!(reqbody.isEmpty())) {	
			StartRequestModel reqmodel = gson.fromJson(reqbody, StartRequestModel.class);
			System.out.println("Start request model--"+ reqmodel);
			String resp = SandboxDriver.startThread(reqmodel);
			//return "Data Generator started Successfully";
			return resp;
		}else return "Error while starting data generator!!!!";
		

	}


	
	private void handleThreadStartResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}
	
	
	private String handleThreadStopRequest(String reqbody) {
		System.out.println("Stopping request body ->");
		String id = gson.fromJson(reqbody, StartRequestModel.class).getNetid();
		String resp = SandboxDriver.stopThread(id);
//	return "Data Generator stopped Successfully";
		return resp;


}



private void handleThreadStopResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
	OutputStream outputStream = httpExchange.getResponseBody();
	String htmlResponse = requestParamValue.toString();
	// this line is a must
	httpExchange.sendResponseHeaders(200, htmlResponse.length());
	outputStream.write(htmlResponse.getBytes());
	outputStream.flush();
	outputStream.close();
}

}
