package ndp.requestHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class TopologyRequestHandler implements HttpHandler {
	private static String topology="";
	
	@Override 
	public void handle(HttpExchange httpExchange) throws IOException {
		String requestParamValue=null; 
		if("GET".equals(httpExchange.getRequestMethod())) { 
			requestParamValue = handleGetRequest(httpExchange);
			handleGetResponse(httpExchange,requestParamValue); 
		}else if("POST".equals(httpExchange.getRequestMethod())) { 
			System.out.println(" post request");
			InputStream is = httpExchange.getRequestBody();
			try (
	                InputStreamReader isr = new InputStreamReader(is,
	                        StandardCharsets.UTF_8);
	                BufferedReader br = new BufferedReader(isr)) {

	            br.lines().forEach(line -> System.out.println(line));
	        }
			
			requestParamValue = handlePostRequest(httpExchange);   
			System.out.println(" requestParamValue");
			handlePostResponse(httpExchange,requestParamValue); 

		}  
		
	}


	private String handleGetRequest(HttpExchange httpExchange) throws IOException {
		String htmlResponse = getTopology();
		return htmlResponse;

	}
	private String handlePostRequest(HttpExchange httpExchange) {
		//return httpExchange.getRequestBody().toString();
		return "Notification Received Successfully";

	}

	private void handleGetResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}
	
	private void handlePostResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}


	public static String getTopology() {
		return topology;
	}


	public static void setTopology(String topology) {
		TopologyRequestHandler.topology = topology;
	}

}
