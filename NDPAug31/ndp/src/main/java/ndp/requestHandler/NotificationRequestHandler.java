package ndp.requestHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import ndp.controller.SandboxDriver;
import ndp.networkAdapter.ConfigChangeNotification;
import ndp.networkAdapter.NotificationAck;

public class NotificationRequestHandler implements HttpHandler {
	private static String notification="";
	private static boolean configChangeFlag = false;


	@Override 
	public void handle(HttpExchange httpExchange) throws IOException {
		String requestParamValue=null; 
		/*	if("GET".equals(httpExchange.getRequestMethod())) { 
			requestParamValue = handleGetRequest(httpExchange);
			handleGetResponse(httpExchange,requestParamValue); 
		}else */if("POST".equals(httpExchange.getRequestMethod())) { 
			System.out.println(" post request");
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
			InputStream is = httpExchange.getRequestBody();
			StringBuilder bodyStr = new StringBuilder();
			try (
					InputStreamReader isr = new InputStreamReader(is,
							StandardCharsets.UTF_8);
					BufferedReader br = new BufferedReader(isr)) {

				br.lines().forEach(line -> bodyStr.append(line));

			}


			System.out.println("body--> " + bodyStr.toString());
			requestParamValue = handlePostRequest(bodyStr.toString());   
			handlePostResponse(httpExchange,requestParamValue); 

			//setNotification(bodyStr.toString());
			//	handlePostResponse(httpExchange,requestParamValue); 

		}  

	}

	/*
	private String handleGetRequest(HttpExchange httpExchange) throws IOException {
		String htmlResponse = getNotification();
		return htmlResponse;

	}
	 */
	private String handlePostRequest(String reqbody) {
		//return httpExchange.getRequestBody().toString();
	System.out.println("Notification Req Body" + reqbody);
		Gson gson = new Gson();
		String resp="";
		if(reqbody.contains("CONFIG_CHANGE")) {
			ConfigChangeNotification cmnotification = gson.fromJson(reqbody, ConfigChangeNotification.class);
			setNotification(reqbody);
			setConfigChangeFlag(true);
			System.out.println("Setting flag true" + reqbody);
			resp = SandboxDriver.configupdate(cmnotification);
			//SandboxDriver.sendNextRopData(cmnotification.getNetworkId()); direct cm from rapp

		}else if(reqbody.contains("TOPOLOGY")) {
			//	O1Notification o1notf = gson.fromJson(reqbody, O1Notification.class);
			NotificationAck ack = gson.fromJson(reqbody, NotificationAck.class);
			resp = reqbody;
			System.out.println("Notification received:" + reqbody);
			if(ack.getStatus().equalsIgnoreCase("success")) {
				System.out.println("Now You Can Start the Data Generation Process !!!!");
			}else 
				{
				System.out.println("Failure in ack , please dont start Data Generation Process !!!!");
				}
		} else if(reqbody.contains("CM"))
		{
			NotificationAck ack = gson.fromJson(reqbody, NotificationAck.class);
			System.out.println("Notification received:" + reqbody);

			if(ack.getStatus().equalsIgnoreCase("success")) {
				System.out.println("Config Changes Applied Successfully !!!!");
			}else { 
				System.out.println("Failure in ack , please stop Data Generation Process !!!!");			
				SandboxDriver.stopThread(ack.getNetworkId());
				//resp = reqbody;
				resp = "Stopping Data Generator!!!";
			}
		}
		/* if trigger comes direct from rapp
		 else if(reqbody.contains("NO_CONFIG_CHANGE"))
		{
			NotificationAck ack = gson.fromJson(reqbody, NotificationAck.class);
			System.out.println("Notification received:" + reqbody);

			if(ack.getStatus().equalsIgnoreCase("success")) {
				System.out.println("Config Changes Applied Successfully !!!!");
				SandboxDriver.sendNextRopData(ack.getNetworkId());
				//send next rop data;
			}else { 
				System.out.println("Failure in ack , please stop Data Generation Process !!!!");			
				SandboxDriver.stopThread(ack.getNetworkId());
				//resp = reqbody;
				resp = "Stopping Data Generator!!!";
			}
		}*/
		return "Notification Received Successfully"+ resp;

	}

		//	EsdbClient esclient = new EsdbClient();
		//	String search = genertaeSearch(o1notf.getNetworkId(),o1notf.getCellId(), o1notf.getFieldName().get(0));
		//String topostr = esclient.fetchData(o1notf.getDbIndex(), o1notf.getDocIdStart());
		//	String topostr = esclient.matchQuery(search, o1notf.getDbIndex());
		//	System.out.println("topo str " + topostr);

		//	JsonObject topo = gson.fromJson(topostr, JsonObject.class).get("_source").getAsJsonObject();
		//	JsonObject topo = gson.fromJson(topostr, JsonObject.class).get("hits").getAsJsonObject();

		//System.out.println("topo devices " + topo.entrySet());
		//	JsonObject innertopo = topo.get("hits").getAsJsonArray().get(0).getAsJsonObject();
		//	System.out.println("topo devices " + innertopo.get("_source"));
		//}
		
	/*
	private void handleGetResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}
	 */
	private void handlePostResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}


	public static String getNotification() {
		return notification;
	}


	public static void setNotification(String notification) {
		NotificationRequestHandler.notification = notification;
	}

	private String generateSearch(String path, String value, String fetchkey) {

		String search = "{\n" + 
				"  \"size\" : 1,\n" + 
				"  \"query\" : {\n" + 
				"      \"bool\":{\n" + 
				"      \"must\":{\n" + 
				"        \"match\" : {\n" + 
				//	"        \"topology.device.attributes.DUID\" : {\n" + 
				//	"        \"query\" : \"30001\",\n" + 
				//	"        \"topology.enodebs.cells.cellId\" : {\n" + 
				//	"        \"query\" : \"STL0001_0\",\n" + 
				// 	"      \"topology.device.attributes.cells.cellId\" : {\n" + 
				"      \""+path+"\" : {\n" + 

			// 	"	  \"query\" : \"4467105667940008\",\n" + 
			"	  \"query\" : \""+value+"\",\n" + 

				"        \"operator\" : \"AND\",\n" + 
				"        \"prefix_length\" : 0,\n" + 
				"        \"max_expansions\" : 50,\n" + 
				"        \"fuzzy_transpositions\" : true,\n" + 
				"        \"lenient\" : false,\n" + 
				"        \"zero_terms_query\" : \"NONE\",\n" + 
				"        \"boost\" : 1.0\n" + 
				"      }\n" + 
				"        }\n" + 
				"      },\n" + 
				"      \"should\":[\n" + 
				"    		{\n" + 
				//		"          \"match\": {\"topology.device.attributes.DUID\": \"30001 \"}\n" + 
				//		"         \"match\": {\"topology.device.attributes.cells.cellId\": \"4467105667940008 \"}\n" + 
				"         \"match\": {\""+path+"\": \""+value+"\"}\n" + 

				"        }\n" + 
				"    		]\n" + 
				"      \n" + 
				"      }\n" + 
				"    },\n" + 
				"   \"_source\" : {\n" + 
				"    \"includes\" : [\n" + 
				//		"     \"topology.device.attributes.DUID\",\n" + 
				//		"      \"topology.device.attributes.cells.cellId\",\n" + 
				"      \""+path+"\",\n" + 
				"      \""+fetchkey+"\"\n" + 

		//		"      \"topology.device.attributes.cells.energySavingControl\"\n" + 
		//		"      \"topology.enodebs.cells.latitude\"\n" + 

				"    ],\n" + 
				"    \"excludes\" : [\n" + 
				"      \"\"\n" + 
				"    ]\n" + 
				"  }\n" + 
				"}\n" + 
				"";
		return search;

	}

	public static boolean isConfigChangeFlag() {
		return configChangeFlag;
	}

	public static void setConfigChangeFlag(boolean configChangeFlag) {
		NotificationRequestHandler.configChangeFlag = configChangeFlag;
	}


}
