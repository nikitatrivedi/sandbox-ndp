package ndp.requestHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import ndp.controller.PmDataModel;
import ndp.controller.SandboxDriver;

public class EsPmDataRequestHandler implements HttpHandler {
	Gson gson = new Gson();

	
	@Override 
	public void handle(HttpExchange httpExchange) throws IOException {
		String requestParamValue=null;  
		 if("POST".equals(httpExchange.getRequestMethod())) { 
			
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
			InputStream is = httpExchange.getRequestBody();
			StringBuilder bodyStr = new StringBuilder();
			try (
	                InputStreamReader isr = new InputStreamReader(is,
	                        StandardCharsets.UTF_8);
	                BufferedReader br = new BufferedReader(isr)) {
	            br.lines().forEach(line -> bodyStr.append(line));	            
	        }		
			
				System.out.println(" received pm data from dg");
				
			handleReceivePmDataResponse(httpExchange); 
			 handleReceivePmDataRequest(bodyStr.toString());   			
			
		}  
	}

	private void handleReceivePmDataRequest(String reqbody) {
		//if(!(reqbody.isEmpty())) {	
			//System.out.println("PM-->" + reqbody);
			//ObjectMapper obj = new ObjectMapper();
			PmDataModel reqmodel = gson.fromJson(reqbody, PmDataModel.class);
			//@SuppressWarnings("unchecked")
			//ArrayList<EsPmDataModel> reqmodel = gson.fromJson(reqbody, ArrayList.class);

			System.out.println("calling process pm data" + reqmodel.getId());

			SandboxDriver.processPmData(reqmodel.getId(),reqmodel.getData());
			//return "Pm Received Successfully";
		//}else return "Emp!!!!";
		

	}
	
	private void handleReceivePmDataResponse(HttpExchange httpExchange)  throws  IOException {
		String requestParamValue = "success";
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}
}
