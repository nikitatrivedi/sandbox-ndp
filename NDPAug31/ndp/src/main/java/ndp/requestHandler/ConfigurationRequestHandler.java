package ndp.requestHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import ndp.controller.Configuration;
import ndp.controller.SandboxDriver;
import ndp.controller.SandboxWorker;
import ndp.controller.StartRequestModel;
import ndp.networkAdapter.ConfigChangeNotification;

public class ConfigurationRequestHandler implements HttpHandler {
	Gson gson = new Gson();

	@Override 
	public void handle(HttpExchange httpExchange) throws IOException {
		String requestParamValue=null;  
		if("GET".equals(httpExchange.getRequestMethod())) { 		
			System.out.println(" get conf request");
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");

			requestParamValue = handleConfGetRequest();   			
			handleConfGetResponse(httpExchange,requestParamValue); 

		}else if("POST".equals(httpExchange.getRequestMethod())) { 	
			System.out.println(" add conf request"+ httpExchange.getRequestURI().getPath());
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
			InputStream is = httpExchange.getRequestBody();
			StringBuilder bodyStr = new StringBuilder();
			try (
					InputStreamReader isr = new InputStreamReader(is,
							StandardCharsets.UTF_8);
					BufferedReader br = new BufferedReader(isr)) {
				br.lines().forEach(line -> bodyStr.append(line));	            
			}					
			//requestParamValue = handleConfStartRequest(bodyStr.toString());   			
			handleConfStartResponse(httpExchange,bodyStr.toString());
			handleConfStartRequest(bodyStr.toString());   			

		}  
		else if("DELETE".equals(httpExchange.getRequestMethod())) { 
			System.out.println("Delete Request-> ");
			String id = httpExchange.getRequestURI().toString().split("\\?")[1].split("=")[1];
			httpExchange.getResponseHeaders().set("Content-type", "application/json; charset=UTF-8");
			InputStream is = httpExchange.getRequestBody();
			requestParamValue = handleConfDelRequest(id);   			
			handleConfDelResponse(httpExchange,requestParamValue);   
		}
	}



	private String handleConfStartRequest(String reqbody) {
		System.out.println("Start request body--"+ reqbody);
		
		if(!(reqbody.isEmpty())) {	
			Configuration reqmodel = gson.fromJson(reqbody, Configuration.class);
			System.out.println("Add conf model--"+ reqmodel);
			String response = SandboxDriver.addConfiguration(reqmodel).toString();
			return response;
		}else return "Error while adding configurations!!!!";


	}

	private void handleConfStartResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
	OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}

	private String handleConfDelRequest(String id) {
		String response = SandboxDriver.deleteConfiguration(id);
		return response;
	}

	private void handleConfDelResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}

	private String handleConfGetRequest() {
		String resp = SandboxDriver.getConfiguration();
		return resp;

	}

	private void handleConfGetResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {
		OutputStream outputStream = httpExchange.getResponseBody();
		String htmlResponse = requestParamValue.toString();
		// this line is a must
		httpExchange.sendResponseHeaders(200, htmlResponse.length());
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}
}
