package ndp.topology.RanAuto;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class enodeb {
	private String enodebId;
	private String timestamp;
	private String enodebName;
	private String siteId;
	private String siteLatitude;
	private String siteLongitude;
	private Set<String> pLMNIdList = new HashSet<String>();
	private ArrayList<cell> cells = new ArrayList<cell>();
	public String getEnodebId() {
		return enodebId;
	}
	public void setEnodebId(String enodebId) {
		this.enodebId = enodebId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getEnodebName() {
		return enodebName;
	}
	public void setEnodebName(String enodebName) {
		this.enodebName = enodebName;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getSiteLatitude() {
		return siteLatitude;
	}
	public void setSiteLatitude(String siteLatitude) {
		this.siteLatitude = siteLatitude;
	}
	public String getSiteLongitude() {
		return siteLongitude;
	}
	public void setSiteLongitude(String siteLongitude) {
		this.siteLongitude = siteLongitude;
	}
	public Set<String> getpLMNIdList() {
		return pLMNIdList;
	}
	public void setpLMNIdList(Set<String> pLMNIdList) {
		this.pLMNIdList = pLMNIdList;
	}
	public ArrayList<cell> getCells() {
		return cells;
	}
	public void setCells(ArrayList<cell> cells) {
		this.cells = cells;
	}
	
	
	
}
