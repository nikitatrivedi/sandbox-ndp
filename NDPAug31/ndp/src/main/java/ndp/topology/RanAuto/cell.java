package ndp.topology.RanAuto;

import java.util.ArrayList;

public class cell {
	private String cellId;
	private String cellName;
	private String latitude;
	private String longitude;
	private String relatedSector;
	private String pLMNId;
	private String beamwidth;
	private String opBandCode;
	private String earfcnDl;
	private String azimuth;
	private ArrayList<String> nbrList = new ArrayList<String>();
	
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	public String getCellName() {
		return cellName;
	}
	public void setCellName(String cellName) {
		this.cellName = cellName;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public ArrayList<String> getNbrList() {
		return nbrList;
	}
	public void setNbrList(ArrayList<String> nbrList) {
		this.nbrList = nbrList;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getRelatedSector() {
		return relatedSector;
	}
	public void setRelatedSector(String relatedSector) {
		this.relatedSector = relatedSector;
	}
	public String getpLMNId() {
		return pLMNId;
	}
	public void setpLMNId(String pLMNId) {
		this.pLMNId = pLMNId;
	}
	public String getBeamwidth() {
		return beamwidth;
	}
	public void setBeamwidth(String beamwidth) {
		this.beamwidth = beamwidth;
	}
	public String getOpBandCode() {
		return opBandCode;
	}
	public void setOpBandCode(String opBandCode) {
		this.opBandCode = opBandCode;
	}
	public String getEarfcnDl() {
		return earfcnDl;
	}
	public void setEarfcnDl(String earfcnDl) {
		this.earfcnDl = earfcnDl;
	}
	public String getAzimuth() {
		return azimuth;
	}
	public void setAzimuth(String azimuth) {
		this.azimuth = azimuth;
	}
	
}
