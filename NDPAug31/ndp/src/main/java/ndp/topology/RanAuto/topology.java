package ndp.topology.RanAuto;
import java.util.ArrayList;

public class topology {
	private String networkType;
	private String networkId;
	private ArrayList<enodeb> enodebs = new ArrayList<enodeb>();
	
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public ArrayList<enodeb> getEnodebs() {
		return enodebs;
	}
	public void setEnodebs(ArrayList<enodeb> enodebs) {
		this.enodebs = enodebs;
	}
	
	
}
