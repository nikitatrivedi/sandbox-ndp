package ndp.topology.RanAuto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.FormulaEvaluator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class InputFileReaderRanAuto {

	private String networkId = "";
	
	private static String mcc = "312";
	private static String mnc = "390";
	private String topojson="";
	private static String filePath = "";
	private String starttime = "";
	private static AtomicInteger netnum = new AtomicInteger();


	public String generateRanAutoTopology(String path) {
		topology topo = new topology();
		//int netnum = new Random().nextInt(100);
		int netnumtemp = netnum.incrementAndGet();
		System.out.println("Network id---> " +"4g-radio-network-"+netnumtemp );
		topo.setNetworkId("lte-radio-network-"+netnumtemp);
		
		String time = Instant.now().toString();
		//topo.setNetworkId("4g-radio-network-1");
		setNetworkId(topo.getNetworkId());
		topo.setNetworkType("LTE");
		setStarttime(time);
		try {
			//FileInputStream fis=new FileInputStream(new File("/home/ubuntu/eclipse-workspace/SandboxIntegrationv1/PythonNDP/Pythonfiles/Farmers_Site_Sector_v2.xls"));
			FileInputStream fis=new FileInputStream(new File(path));

			HSSFWorkbook wb=new HSSFWorkbook(fis);
			HSSFSheet sheet=wb.getSheetAt(0); 
			//evaluating cell type   
			//FormulaEvaluator formulaEvaluator=wb.getCreationHelper().createFormulaEvaluator();  
			wb.close();
			Set<String> enblist = new HashSet<String>() ;
			
			HashMap<String,HashMap<String,String>> enbmap = new HashMap<String,HashMap<String,String>>();
			//for(Row row: sheet)     //iteration over row using for each loop 
			for (int i = 1; i<=sheet.getLastRowNum(); i++ )
			{  
				String enbid = sheet.getRow(i).getCell(1).getStringCellValue();
				enblist.add(enbid);
				HashMap<String,String> innermap = new HashMap<String,String>();
				innermap.put("siteid", sheet.getRow(i).getCell(0).getStringCellValue());
				innermap.put("latitude", String.valueOf(sheet.getRow(i).getCell(3).getNumericCellValue()));
				innermap.put("longitude", String.valueOf(sheet.getRow(i).getCell(4).getNumericCellValue()));
				enbmap.put(enbid, innermap);
				
			//for(Cell cell: row)    //iteration over cell using for each loop  
			//{  
				//System.out.println(); 
			//}  
			 
			}  
			//System.out.println(enblist.size()); 
			ArrayList<enodeb> enodebs = new ArrayList<enodeb>();
			Iterator<String> itr = enblist.iterator();
			
			while(itr.hasNext()) {
				enodeb enb = new enodeb();
				String id = itr.next();
				Set<String> plmnlist = new HashSet<String>();
				enb.setEnodebId(id);
				enb.setEnodebName(id);
				enb.setSiteId(enbmap.get(id).get("siteid"));
				enb.setSiteLatitude(enbmap.get(id).get("latitude"));
				enb.setSiteLongitude(enbmap.get(id).get("longitude"));
				enb.setTimestamp(getStarttime());
				ArrayList<cell> cells = new ArrayList<cell>();
				for (int i = 1; i<=sheet.getLastRowNum(); i++ ) {
					if(id==sheet.getRow(i).getCell(1).getStringCellValue()) {
						
						cell cell = new cell();
						cell.setCellId(sheet.getRow(i).getCell(2).getStringCellValue());
						cell.setCellName(sheet.getRow(i).getCell(2).getStringCellValue());
						cell.setLatitude(String.valueOf(sheet.getRow(i).getCell(3).getNumericCellValue()));
						cell.setLongitude(String.valueOf(sheet.getRow(i).getCell(4).getNumericCellValue()));
						double azim = sheet.getRow(i).getCell(6).getNumericCellValue();
						cell.setAzimuth(String.valueOf((int) azim));
						double earfcnnum = sheet.getRow(i).getCell(5).getNumericCellValue();
						cell.setEarfcnDl(String.valueOf( (int) earfcnnum));
						cell.setBeamwidth("120");
						String[] strarray = cell.getCellId().split("_");
						cell.setOpBandCode(String.valueOf(strarray[1].charAt(0)));
						cell.setRelatedSector(String.valueOf(strarray[1].charAt(1)));
						cell.setpLMNId(mcc+mnc);
						plmnlist.add(cell.getpLMNId());
						cells.add(cell);
					}
				}
				enb.setCells(cells);
				enb.setpLMNIdList(plmnlist);
				enodebs.add(enb);
				
			}
			topo.setEnodebs(enodebs);
			ObjectMapper obj = new ObjectMapper();
				String jsonstringconfig = obj.enable(SerializationFeature.WRAP_ROOT_VALUE).writerWithDefaultPrettyPrinter().writeValueAsString(topo);
				//System.out.println("JSON"+ jsonstringconfig);
			//String jsonstringconfig = obj.writeValueAsString(topo);
				topojson=jsonstringconfig;
				//obj.enable(SerializationFeature.WRAP_ROOT_VALUE);
			//	File file = new File("RanAutoTopology"+getNetworkId()+".json");
				File file = new File(getNetworkId()+".json");

				setFilePath(file.getAbsolutePath());
			
				obj.writerWithDefaultPrettyPrinter().writeValue(new FileWriter(file.getAbsolutePath()), topo);
		} catch (IOException e) {
			System.out.println("Exception occured while creating topology" + e.getLocalizedMessage());
		}  
		return topojson;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

}
