package ndp.topology.es;

public class cuup {
	
	private String latitude;
	private String longitude;
	private String gnbId;
	private String userDefinedState;
	private String vendorName;
	private String CUUPId;
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getGnbId() {
		return gnbId;
	}
	public void setGnbId(String gnbId) {
		this.gnbId = gnbId;
	}
	public String getUserDefinedState() {
		return userDefinedState;
	}
	public void setUserDefinedState(String userDefinedState) {
		this.userDefinedState = userDefinedState;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getCUUPId() {
		return CUUPId;
	}
	public void setCUUPId(String cUUPId) {
		CUUPId = cUUPId;
	}
	
	

}
