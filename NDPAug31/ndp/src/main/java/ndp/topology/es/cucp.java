package ndp.topology.es;

import java.util.ArrayList;

public class cucp {
	private String latitude;
	private String longitude;
	private String gnbId;
	private String pLMNId;
	private String CUCPId;
	private String CUCPName;
	private String userDefinedState;
	private String vendorName;
	private ArrayList<String> xnConnections = new ArrayList<String>();
	
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getGnbId() {
		return gnbId;
	}
	public void setGnbId(String gnbId) {
		this.gnbId = gnbId;
	}
	public String getpLMNId() {
		return pLMNId;
	}
	public void setpLMNId(String pLMNId) {
		this.pLMNId = pLMNId;
	}
	public String getCUCPId() {
		return CUCPId;
	}
	public void setCUCPId(String cUCPId) {
		CUCPId = cUCPId;
	}
	public String getCUCPName() {
		return CUCPName;
	}
	public void setCUCPName(String cUCPName) {
		CUCPName = cUCPName;
	}
	public String getUserDefinedState() {
		return userDefinedState;
	}
	public void setUserDefinedState(String userDefinedState) {
		this.userDefinedState = userDefinedState;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public ArrayList<String> getXnConnections() {
		return xnConnections;
	}
	public void setXnConnections(ArrayList<String> xnConnections) {
		this.xnConnections = xnConnections;
	}
	
	
}
