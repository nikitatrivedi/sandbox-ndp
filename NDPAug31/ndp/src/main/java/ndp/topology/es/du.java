package ndp.topology.es;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class du {
	private String latitude;
	private String longitude;
	private String gnbId;
	private String DUId;
	private String DUName;
	private String userDefinedState;
	private String vendorName;
	private Set<String> CUUPs = new HashSet<String>();
	private ArrayList<cell> cells = new ArrayList<cell>();
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getGnbId() {
		return gnbId;
	}
	public void setGnbId(String gnbId) {
		this.gnbId = gnbId;
	}
	public String getDUId() {
		return DUId;
	}
	public void setDUId(String dUId) {
		DUId = dUId;
	}
	public String getDUName() {
		return DUName;
	}
	public void setDUName(String dUName) {
		DUName = dUName;
	}
	public String getUserDefinedState() {
		return userDefinedState;
	}
	public void setUserDefinedState(String userDefinedState) {
		this.userDefinedState = userDefinedState;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public Set<String> getCUUPs() {
		return CUUPs;
	}
	public void setCUUPs(Set<String> cUUPs) {
		CUUPs = cUUPs;
	}
	public ArrayList<cell> getCells() {
		return cells;
	}
	public void setCells(ArrayList<cell> cells) {
		this.cells = cells;
	}
}
