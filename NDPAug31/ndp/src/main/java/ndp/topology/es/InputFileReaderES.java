package ndp.topology.es;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.internal.LinkedTreeMap;

import ndp.networkAdapter.ConfigChangeNotification;
import ndp.networkAdapter.ConfigData;

public class InputFileReaderES {
	private static network5g net = new network5g();
	private static String networkId = "";
	private static HashMap<String,String> cellGnbmap = new HashMap<String,String>();
	private static String arfcndl_cov = "156100";
	private static String arfcnul_cov = "145100";
	private static String arfcndl_cap = "428000";
	private static String arfcnul_cap = "39000";
	private static String mcc = "404";//001
	private static String mnc = "010";//01
	private static ObjectMapper obj = new ObjectMapper();
	private String topojson = "";
	private String starttime = "";
	private static int relationId =0;
	private static String filePath = "";
	private static Set<String> networkIdSet = new HashSet<String>();
	private static AtomicInteger netnum = new AtomicInteger();

	private ArrayList<ConfigData> cmconfigAck;
	private ConfigChangeNotification cmackobj;

	public static network5g getNet() {
		return net;
	}
	public static void setNet(network5g net) {
		InputFileReaderES.net = net;
	}
	public String getTopojson() {
		return topojson;
	}
	public void setTopojson(String topojson) {
		this.topojson = topojson;
	}
	
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public ArrayList<ConfigData> getCmconfigAck() {
		return cmconfigAck;
	}
	public void setCmconfigAck(ArrayList<ConfigData> cmconfigAck) {
		this.cmconfigAck = cmconfigAck;
	}

	/*public static void main(String[] args) {
		try {
			net = createTopologyJson("/home/ubuntu/eclipse-workspace/SandboxIntegrationv1/PythonNDP/Pythonfiles/5GData.xls");
			ObjectMapper obj = new ObjectMapper();
			String jsonstringconfig = obj.writerWithDefaultPrettyPrinter().writeValueAsString(net);
			//System.out.println("JSON"+ jsonstringconfig);


		net = updateTopology("00101030", "energySavingControl", "toBeEnergySaving");
		jsonstringconfig = obj.writerWithDefaultPrettyPrinter().writeValueAsString(net);
		System.out.println("updatedJSON"+ jsonstringconfig);
		}catch (IOException e) {
			e.printStackTrace();
		}  
	}*/

	public ConfigChangeNotification getCmackobj() {
		return cmackobj;
	}
	public void setCmackobj(ConfigChangeNotification cmackobj) {
		this.cmackobj = cmackobj;
	}
	public InputFileReaderES() {

	}
	//	private network5g createTopologyJson(String path) {
	public String createTopologyJson(String path) {

		network5g net1 = new network5g();
		
		
		//int netnum = new Random().nextInt(100);
		int netnumtemp = netnum.incrementAndGet();
		
		String networkidtemp = "5g-radio-network-"+netnum;
		System.out.println("Network id---> " +"5g-radio-network-"+netnumtemp );
		
			net1.setNetworkId("5g-radio-network-"+netnumtemp);
			networkIdSet.add(networkidtemp);
		
		
		//net1.setNetworkId("5g-radio-network-1");
		
		net1.setNetworkType("5G");
		setNetworkId(net1.getNetworkId());
//		setStarttime(time);
		FileInputStream fis;
		try {
			fis = new FileInputStream(new File(path));

			HSSFWorkbook wb=new HSSFWorkbook(fis);
			HSSFSheet sheet=wb.getSheetAt(0); 
			//FormulaEvaluator formulaEvaluator=wb.getCreationHelper().createFormulaEvaluator();  
			wb.close();

			//Set<String> culist = new HashSet<String>() ;
			HashMap<String,String> cucpmap = new HashMap<String,String>();
			HashMap<String,String> cuupmap = new HashMap<String,String>();
			HashMap<String,HashMap<String,String>> dumap = new HashMap<String,HashMap<String,String>>();
			Set<String> duidlist = new HashSet<String>();

			ArrayList<device> topology = new ArrayList<device>();
			HashMap<String,ArrayList<cell>> ducellList = new HashMap<String,ArrayList<cell>>();
			for (int i = 1; i<=sheet.getLastRowNum(); i++ )
			{  
				
				String cucpid = sheet.getRow(i).getCell(0).getStringCellValue();
				String gnbid = String.valueOf(sheet.getRow(i).getCell(1).getNumericCellValue()).split("\\.")[0];
				cucpmap.put(cucpid, gnbid);
				String cuupid = String.valueOf(sheet.getRow(i).getCell(2).getNumericCellValue()).split("\\.")[0];
				cuupmap.put(cuupid, gnbid);

				String duid = String.valueOf(sheet.getRow(i).getCell(3).getNumericCellValue()).split("\\.")[0];
				
				HashMap<String,String> duinnermap = new HashMap<String,String>();
				duinnermap.put("latitude", String.valueOf(sheet.getRow(i).getCell(8).getNumericCellValue()));
				duinnermap.put("longitude", String.valueOf(sheet.getRow(i).getCell(9).getNumericCellValue()));
				duinnermap.put("gnbid", gnbid);

				dumap.put(duid, duinnermap);
				duidlist.add(duid);	

			}
			for(String key:cucpmap.keySet()) {
				device dev = new device();
				deviceObject cucp = new deviceObject();
				cucp.setType("cucp");
				cucp cucpattr = new cucp();
				//cucpattr.setCUCPId(key);
				// set cucpid as gnbid
				cucpattr.setCUCPId(cucpmap.get(key));
				cucpattr.setCUCPName(key);
				cucpattr.setGnbId(cucpmap.get(key));
				cucpattr.setUserDefinedState("ACTIVE");
				cucpattr.setVendorName("STL-Access");
				cucpattr.setLatitude("-6.3631");
				cucpattr.setLongitude("53.318");
				cucpattr.setpLMNId(mcc+mnc);
				cucp.setAttributes(cucpattr);
				dev.setDevice(cucp);
				topology.add(dev);
			}

			for(String key:cuupmap.keySet()) {
				device dev = new device();
				deviceObject cuup = new deviceObject();
				cuup.setType("cuup");
				cuup cuupattr = new cuup();
				cuupattr.setCUUPId(key);
				cuupattr.setGnbId(cuupmap.get(key));
				cuupattr.setUserDefinedState("ACTIVE");
				cuupattr.setVendorName("STL-Access");
				cuupattr.setLatitude("-6.3631");
				cuupattr.setLongitude("53.318");
				cuup.setAttributes(cuupattr);
				dev.setDevice(cuup);
				topology.add(dev);
			}
			Iterator<String> duiditr = duidlist.iterator();
			while(duiditr.hasNext()) {
				device dev = new device();
				deviceObject du = new deviceObject();
				du.setType("du");
				du duattr = new du();
				String duid = duiditr.next();
				Set<String> cuups = new HashSet<String>();

				duattr.setDUId(duid);
				duattr.setGnbId(dumap.get(duid).get("gnbid"));
				duattr.setLatitude(dumap.get(duid).get("latitude"));
				duattr.setLongitude(dumap.get(duid).get("longitude"));
				duattr.setUserDefinedState("ACTIVE");
				duattr.setDUName("duserver"+duid);
				duattr.setVendorName("STL-Radio");
				ArrayList<cell> cells = new ArrayList<cell>();
				for (int i = 1; i<=sheet.getLastRowNum(); i++ ) {
				//	if(duid==sheet.getRow(i).getCell(3).getStringCellValue()) {
					if(duid.equals(String.valueOf(sheet.getRow(i).getCell(3).getNumericCellValue()).split("\\.")[0])) {

						//cuups.add(sheet.getRow(i).getCell(2).getStringCellValue());
						cuups.add(String.valueOf(sheet.getRow(i).getCell(2).getNumericCellValue()).split("\\.")[0]);

						cell cell = new cell();
						//cell.setCellId(sheet.getRow(i).getCell(4).getStringCellValue());
						cell.setCellName(sheet.getRow(i).getCell(7).getStringCellValue());
						cell.setLatitude(String.valueOf(sheet.getRow(i).getCell(8).getNumericCellValue()));
						cell.setLongitude(String.valueOf(sheet.getRow(i).getCell(9).getNumericCellValue()));
						cell.setCellAzimuth(String.valueOf(sheet.getRow(i).getCell(10).getNumericCellValue()).split("\\.")[0]);
						cell.setBsChannelBwDL("20");
						cell.setArfcnUL(String.valueOf(sheet.getRow(i).getCell(6).getNumericCellValue()).split("\\.")[0]);
						cell.setAdministrativeState("UNLOCKED");
						cell.setLocalId(String.valueOf(i));
						//cell.setLocalId(sheet.getRow(i).getCell(4).getStringCellValue());
						cell.setPci(String.valueOf(i));
						cell.setOperationalState("ENABLED");
						cell.setCesSwitch("true");
						int gnbid = (int) sheet.getRow(i).getCell(1).getNumericCellValue();
						int plmn = Integer.parseInt(mcc+mnc);
						int gnbidlength =22;
						//String cellid = generateNRCGI(plmn,gnbid , gnbidlength, Integer.parseInt(cell.getLocalId()));
						//String cellid = sheet.getRow(i).getCell(4).getStringCellValue();
						String cellid =new BigDecimal(sheet.getRow(i).getCell(4).getNumericCellValue()).toPlainString();
						cell.setCellId(cellid);
						int suffix = Character.getNumericValue(cell.getCellName().charAt((cell.getCellName().length())-1));
						if(suffix == 1) {
							cell.setIsCoverageCell("yes");
							cell.setArfcnUL(arfcnul_cov);
							cell.setArfcnDL(arfcndl_cov);
						}
						else if(suffix==2) {
							cell.setIsCoverageCell("no");
							cell.setArfcnUL(arfcnul_cap);
							cell.setArfcnDL(arfcndl_cap);

						}
						cell.setEnergySavingControl("toBeNotEnergySaving");
						if(cell.getEnergySavingControl().equalsIgnoreCase("toBeNotEnergySaving")) {
							cell.setEnergySavingState("isNotEnergySaving");
							cell.setCellState("Active");
						}else {
							cell.setEnergySavingState("isEnergySaving");
							//cell.setCellState("Inactive");
							cell.setCellState("Active");
						}
						cellGnbmap.put(cell.getCellId(), duattr.getGnbId());
						cells.add(cell);
						//cellMap.put(cell.getCellId(), cell);
						ducellList.put(duid, cells);
						cells = addNeighbors(cells);
						duattr.setCells(cells);

					}
				}
				duattr.setCUUPs(cuups);
				du.setAttributes(duattr);
				dev.setDevice(du);
				topology.add(dev);
			}
			net1.setTopology(topology);
			String jsonstringconfig = obj.writerWithDefaultPrettyPrinter().writeValueAsString(net1);
			File file = new File(getNetworkId()+".json");

			setFilePath(file.getAbsolutePath());
			obj.writerWithDefaultPrettyPrinter().writeValue(new FileWriter(file), net1);
			topojson = jsonstringconfig;
			//topojson = net1.toString();
		} catch (IOException | NumberFormatException /*|  InvalidInputException */ e ) {
			e.printStackTrace();
			System.out.println("Exception occured while creating 5G topology from xls file " + e.getLocalizedMessage());
		}
		//	return net1;
		net = net1;
		return topojson;

	}
	//	public static network5g updateTopology(String cellId, String param, String val) {
	public HashMap<String, Object> updateTopology(String cellId, String param, String val) {
		ConfigData cmdata = new ConfigData();
		HashMap<String, Object> respmap = new HashMap<String,Object>();
		network5g tempnet = net;
		ArrayList<device> deviceList = net.getTopology();
		boolean cellflag = false;
		for(int i = 0; i< deviceList.size(); i++) {
			device dev = deviceList.get(i);
			if(dev.getDevice().getType()=="du") {
				//System.out.println("DU device"+ cellId);
				Object dudevattr = dev.getDevice().getAttributes();
				du dudev = (du) dudevattr;
				ArrayList<cell> cellList = dudev.getCells();
				for(int j= 0; j<cellList.size(); j++) {
					cell cellitr = cellList.get(j);
					//System.out.println("cellId-->" + cellitr.getCellId() );
					cmdata.setId(cellId);
					if(cellitr.getCellId().equalsIgnoreCase(cellId.trim()) ||cellId.trim().equalsIgnoreCase(cellitr.getCellId().trim()) || cellitr.getCellId().contentEquals(cellId)) {				
						cmdata.setId(cellId);
						//cmdata.setNrtci(cellitr.getCellId()+cellitr.getPci());
						ArrayList<Object> attributeack = new ArrayList<Object>();
						cellflag = true;
						cellitr.setEnergySavingControl(val);
						if(cellitr.getEnergySavingControl().equalsIgnoreCase("toBeNotEnergySaving")) {
						/*	LinkedTreeMap<String, String> cmparammap2 = new LinkedTreeMap<String, String>();							
							cmparammap2.put("energySavingControl", "toBeNotEnergySaving");
							attributeack.add(cmparammap2);
							*/
							LinkedTreeMap<String, String> cmparammap = new LinkedTreeMap<String, String>();
							cellitr.setEnergySavingState("isNotEnergySaving");
							cmparammap.put("energySavingState", "isNotEnergySaving");
							attributeack.add(cmparammap);							
						/*	LinkedTreeMap<String, String> cmparammap2 = new LinkedTreeMap<String, String>();
							cellitr.setCellState("Active");
							cmparammap2.put("cellState", "Active");
							attributeack.add(cmparammap2);
							*/
							

						}else {
						/*	LinkedTreeMap<String, String> cmparammap2 = new LinkedTreeMap<String, String>();							
							cmparammap2.put("energySavingControl", "toBeEnergySaving");
							attributeack.add(cmparammap2);
							*/
							LinkedTreeMap<String, String> cmparammap = new LinkedTreeMap<String, String>();
							cellitr.setEnergySavingState("isEnergySaving");
							cmparammap.put("energySavingState", "isEnergySaving");
							attributeack.add(cmparammap);							
						/*	LinkedTreeMap<String, String> cmparammap2 = new LinkedTreeMap<String, String>();
							cellitr.setCellState("Inactive");
							cmparammap2.put("cellState", "Inactive");
							attributeack.add(cmparammap2);
							*/
							
						}
						cmdata.setAttributes(attributeack);
						//cmconfigAck.add(cmdata);
					}
					cellList.remove(j);
					cellList.add(j, cellitr);
				}
				dudev.setCells(cellList);
				dev.getDevice().setAttributes(dudev);
			}
			deviceList.set(i, dev);
			tempnet.setTopology(deviceList);
		}
		System.out.println("cm falg" + cellflag);
		respmap.put("flag", cellflag);
		respmap.put("id", cellId);
		respmap.put("cmdata", cmdata);
		
		if(cellflag) {
		InputFileReaderES.net=tempnet;
		try {
			String jsonstringconfig = obj.writerWithDefaultPrettyPrinter().writeValueAsString(tempnet);
			//File file = new File("5GTopology.json");
			File file = new File(getNetworkId()+".json");
			setFilePath(file.getAbsolutePath());
			obj.writerWithDefaultPrettyPrinter().writeValue(new FileWriter(file), tempnet);
			topojson = jsonstringconfig;
		} catch (  IOException e) {
			System.out.println("Exception in creating JSON" + e.getLocalizedMessage());
		}
		}
		//	return tempnet;
	//	return topojson;
		//return cmdata;
		return respmap;

	}

	/*
	  public HashMap<String, Object> updateTopologyAD(String cellId, String param, String val) {
		ConfigData cmdata = new ConfigData();
		HashMap<String, Object> respmap = new HashMap<String,Object>();
		network5g tempnet = net;
		ArrayList<device> deviceList = net.getTopology();
		boolean cellflag = false;
		for(int i = 0; i< deviceList.size(); i++) {
			device dev = deviceList.get(i);
			if(dev.getDevice().getType()=="du") {
				//System.out.println("DU device"+ cellId);
				Object dudevattr = dev.getDevice().getAttributes();
				du dudev = (du) dudevattr;
				ArrayList<cell> cellList = dudev.getCells();
				for(int j= 0; j<cellList.size(); j++) {
					cell cellitr = cellList.get(j);
					//System.out.println("cellId-->" + cellitr.getCellId() );
					cmdata.setId(cellId);
					if(cellitr.getCellId().equalsIgnoreCase(cellId.trim()) ||cellId.trim().equalsIgnoreCase(cellitr.getCellId().trim()) || cellitr.getCellId().contentEquals(cellId)) {				
						cmdata.setId(cellId);
						//cmdata.setNrtci(cellitr.getCellId()+cellitr.getPci());
						ArrayList<Object> attributeack = new ArrayList<Object>();
						cellflag = true;
						cellitr.setEnergySavingControl(val);
						if(cellitr.getEnergySavingControl().equalsIgnoreCase("toBeNotEnergySaving")) {
							LinkedTreeMap<String, String> cmparammap2 = new LinkedTreeMap<String, String>();							
							cmparammap2.put("energySavingControl", "toBeNotEnergySaving");
							attributeack.add(cmparammap2);
							
							LinkedTreeMap<String, String> cmparammap = new LinkedTreeMap<String, String>();
							cellitr.setEnergySavingState("isNotEnergySaving");
							cmparammap.put("energySavingState", "isNotEnergySaving");
							attributeack.add(cmparammap);							
						

						}else {
							LinkedTreeMap<String, String> cmparammap2 = new LinkedTreeMap<String, String>();							
							cmparammap2.put("energySavingControl", "toBeEnergySaving");
							attributeack.add(cmparammap2);
							
							LinkedTreeMap<String, String> cmparammap = new LinkedTreeMap<String, String>();
							cellitr.setEnergySavingState("isEnergySaving");
							cmparammap.put("energySavingState", "isEnergySaving");
							attributeack.add(cmparammap);							
						
							
						}
						cmdata.setAttributes(attributeack);
						//cmconfigAck.add(cmdata);
					}
					cellList.remove(j);
					cellList.add(j, cellitr);
				}
				dudev.setCells(cellList);
				dev.getDevice().setAttributes(dudev);
			}
			deviceList.set(i, dev);
			tempnet.setTopology(deviceList);
		}
		System.out.println("cm falg" + cellflag);
		respmap.put("flag", cellflag);
		respmap.put("id", cellId);
		respmap.put("cmdata", cmdata);
		
		if(cellflag) {
		InputFileReaderES.net=tempnet;
		try {
			String jsonstringconfig = obj.writerWithDefaultPrettyPrinter().writeValueAsString(tempnet);
			//File file = new File("5GTopology.json");
			File file = new File(getNetworkId()+".json");
			setFilePath(file.getAbsolutePath());
			obj.writerWithDefaultPrettyPrinter().writeValue(new FileWriter(file), tempnet);
			topojson = jsonstringconfig;
		} catch (  IOException e) {
			System.out.println("Exception in creating JSON" + e.getLocalizedMessage());
		}
		}
		//	return tempnet;
	//	return topojson;
		//return cmdata;
		return respmap;

	}
	*/
	
	
	
	private static ArrayList<cell> addNeighbors(ArrayList<cell> cellList){
		ArrayList<cell> tempcellList = cellList;
		ArrayList<cell> celllist = cellList;
		for(int i =0; i<celllist.size();i++) {
			cell srcCell = celllist.get(i);
			ArrayList<neighborcell> nbrList = new ArrayList<neighborcell>();
			for(cell tgcellitr:cellList) {
				if(!(srcCell.getCellId().equalsIgnoreCase(tgcellitr.getCellId()))) {
					neighborcell nbrcell = new neighborcell();
					relationId++;
					nbrcell.setAdjacentNRCellRef(tgcellitr.getCellName());
					nbrcell.setGnbId(cellGnbmap.get(tgcellitr.getCellId()));
					//	nbrcell.setnRTCI(tgcellitr.getCellId());
					nbrcell.setPci(tgcellitr.getPci());
					nbrcell.setLocalId(tgcellitr.getPci());
					//	nbrcell.setRelationId(srcCell.getCellName()+"-"+tgcellitr.getCellName());
					nbrcell.setRelationId(String.valueOf(relationId));
					nbrcell.setCellId(tgcellitr.getCellId());

					nbrcell.setIsHOAllowed("true");
					if((srcCell.getCellAzimuth().equalsIgnoreCase(tgcellitr.getCellAzimuth()))) {
						if((tgcellitr.getIsCoverageCell()).equalsIgnoreCase("yes")) {
							nbrcell.setIsESCoveredBy("FULL");	
						}else nbrcell.setIsESCoveredBy("NO");
						nbrcell.setIsRemoveAllowed("false");
					}else {
						nbrcell.setIsESCoveredBy("NO");
						nbrcell.setIsRemoveAllowed("true");
					}
					nbrList.add(nbrcell);
				}
			}
			srcCell.setNeighbours(nbrList);
			tempcellList.set(i, srcCell);
		}

		return tempcellList;
	}
	
	
	public String getFilePath() {
		return filePath;
	}
	public static void setFilePath(String filePath) {
		InputFileReaderES.filePath = filePath;
	}
/*	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
*/	
	public static String generateNRCGI(Integer plmnId, Integer gNBId, Integer gNBIdLength, Integer cellLocalId) throws InvalidInputException {
		if(plmnId == null) {
		throw new InvalidInputException("plmnId can't be null");
		} else if(gNBId == null) {
		throw new InvalidInputException("gNBId can't be null");
		} else if(gNBIdLength == null) {
		throw new InvalidInputException("gNBIdLength can't be null");
		} else if(cellLocalId == null) {
		throw new InvalidInputException("cellLocalId can't be null");
		}

		Long nci = generateNCI(gNBId, gNBIdLength, cellLocalId);
		return String.valueOf(plmnId)+String.valueOf(nci);
		}

		 // Generate NCI
		private static Long generateNCI(Integer gNBId, Integer gNBIdLength, Integer cellLocalId) {
		return gNBId * (long) (Math.pow(2, (36 - gNBIdLength))) + cellLocalId;
		}
	
}
