package ndp.topology.es;

import java.util.ArrayList;

public class cell {
	private String cellId;
	private String cellName;
	private String localId;
	private String pci;
	private String latitude;
	private String longitude;
	private String cellState;
	private String operationalState;
	private String administrativeState;
	private String arfcnDL;
	private String arfcnUL;
	private String cesSwitch;
	private String cellAzimuth;
	//private enum energySavingControl {ToBeEnergySavings, ToNotBeEnergySavings};
	//private enum energySavingState {IsEnergySavings, IsNotEnergySavings};
	private String energySavingControl;
	private String energySavingState;
	
	private String bsChannelBwDL;
	private String isCoverageCell;
	private ArrayList<neighborcell> neighbours = new ArrayList<neighborcell>();
	
	public String getCellName() {
		return cellName;
	}
	public void setCellName(String name) {
		this.cellName = name;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	
	public String getLocalId() {
		return localId;
	}
	public void setLocalId(String localId) {
		this.localId = localId;
	}
	public String getPci() {
		return pci;
	}
	public void setPci(String pci) {
		this.pci = pci;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCellState() {
		return cellState;
	}
	public void setCellState(String cellState) {
		this.cellState = cellState;
	}
	public String getOperationalState() {
		return operationalState;
	}
	public void setOperationalState(String operationalState) {
		this.operationalState = operationalState;
	}
	public String getAdministrativeState() {
		return administrativeState;
	}
	public void setAdministrativeState(String administrativeState) {
		this.administrativeState = administrativeState;
	}
	public String getArfcnDL() {
		return arfcnDL;
	}
	public void setArfcnDL(String arfcnDL) {
		this.arfcnDL = arfcnDL;
	}
	public String getArfcnUL() {
		return arfcnUL;
	}
	public void setArfcnUL(String arfcnUL) {
		this.arfcnUL = arfcnUL;
	}
	public String getCesSwitch() {
		return cesSwitch;
	}
	public void setCesSwitch(String cesSwitch) {
		this.cesSwitch = cesSwitch;
	}
	public String getCellAzimuth() {
		return cellAzimuth;
	}
	public void setCellAzimuth(String cellAzimuth) {
		this.cellAzimuth = cellAzimuth;
	}
	public String getEnergySavingControl() {
		return energySavingControl;
	}
	public void setEnergySavingControl(String energySavingControl) {
		this.energySavingControl = energySavingControl;
	}
	public String getEnergySavingState() {
		return energySavingState;
	}
	public void setEnergySavingState(String energySavingState) {
		this.energySavingState = energySavingState;
	}
	public String getBsChannelBwDL() {
		return bsChannelBwDL;
	}
	public void setBsChannelBwDL(String bsChannelBwDL) {
		this.bsChannelBwDL = bsChannelBwDL;
	}
	public String getIsCoverageCell() {
		return isCoverageCell;
	}
	public void setIsCoverageCell(String isCoverageCell) {
		this.isCoverageCell = isCoverageCell;
	}
	public ArrayList<neighborcell> getNeighbours() {
		return neighbours;
	}
	public void setNeighbours(ArrayList<neighborcell> neighbours) {
		this.neighbours = neighbours;
	}
	
	
	
}
