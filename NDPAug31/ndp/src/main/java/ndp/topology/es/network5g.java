package ndp.topology.es;

import java.util.ArrayList;

public class network5g {
	private String networkType;
	private String networkId;
	private ArrayList<device> topology = new ArrayList<device>();
	
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public ArrayList<device> getTopology() {
		return topology;
	}
	public void setTopology(ArrayList<device> topology) {
		this.topology = topology;
	}
	
}
