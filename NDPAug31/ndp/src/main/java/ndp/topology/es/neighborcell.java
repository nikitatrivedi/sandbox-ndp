package ndp.topology.es;

public class neighborcell {
	// private String nRTCI;
	private String cellId;
	 private String relationId;
	 private String adjacentNRCellRef;
	 private String gnbId;
	 private String pci;
	 private String localId;
	 private String isHOAllowed;
	 private String isRemoveAllowed;
	 private String isESCoveredBy;
	 
/*	public String getnRTCI() {
		return nRTCI;
	}
	public void setnRTCI(String nRTCI) {
		this.nRTCI = nRTCI;
	}
	*/
	public String getRelationId() {
		return relationId;
	}
	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}
	public String getAdjacentNRCellRef() {
		return adjacentNRCellRef;
	}
	public void setAdjacentNRCellRef(String adjacentNRCellRef) {
		this.adjacentNRCellRef = adjacentNRCellRef;
	}
	public String getGnbId() {
		return gnbId;
	}
	public void setGnbId(String gnbId) {
		this.gnbId = gnbId;
	}
	public String getPci() {
		return pci;
	}
	public void setPci(String pci) {
		this.pci = pci;
	}
	public String getLocalId() {
		return localId;
	}
	public void setLocalId(String localId) {
		this.localId = localId;
	}
	public String getIsHOAllowed() {
		return isHOAllowed;
	}
	public void setIsHOAllowed(String isHOAllowed) {
		this.isHOAllowed = isHOAllowed;
	}
	public String getIsRemoveAllowed() {
		return isRemoveAllowed;
	}
	public void setIsRemoveAllowed(String isRemoveAllowed) {
		this.isRemoveAllowed = isRemoveAllowed;
	}
	public String getIsESCoveredBy() {
		return isESCoveredBy;
	}
	public void setIsESCoveredBy(String isESCoveredBy) {
		this.isESCoveredBy = isESCoveredBy;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	 
	@Override
	public String toString() {
		return "\"{cellId ="+ cellId + 
        		", relationId = " + relationId +
        		", adjacentNRCellRef = " + adjacentNRCellRef + 
        		", gnbId = "+ gnbId +
        		", pci = "+ pci +
        		", localId = "+ localId +
        		", isHOAllowed = "+ isHOAllowed +
        		", isESCoveredBy = "+ isESCoveredBy +
        		", isRemoveAllowed = "+ isRemoveAllowed +
        		"} ";    
    }
}
