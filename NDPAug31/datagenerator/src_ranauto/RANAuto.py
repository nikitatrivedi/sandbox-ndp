from datetime import timezone
from random import sample
import datetime
import random
import json
import sys, os
import time
import string
import pandas as pd
import requests
from urllib3.util import current_time
from SandboxNdp import stopdatageneration
#path1=os.getcwd()
#mypath=os.path.abspath(path1)
mypath=os.path.dirname(__file__)
configpath=os.path.join(mypath,"Ran_config.json")
kgppmpath=os.path.join(mypath,"RanAutoPmData.csv")

class RANAuto:
    cellStateStore = {}
    pmDataGlobal = {}
    __inputType = ""
    netid = ""
    sequence = 1
    csvRop = 0
    __pmdata = []
    flag = False
    filepath = ""
    cmCellState = {}
    def setGranularity(self, rop):
        self.granularity = rop
        return self.granularity

    def getCellId(self, cellId):
        self.cellId = cellId

    def setStartDateTime(self, year, month, day, hh, mm, ss,ms=0):
        self.startdatetime = datetime.datetime(year, month, day, hh, mm, ss,ms)
        self.startTime = self.startdatetime
        self.currenttime = self.startTime

    def setStopDateTime(self, year, month, day, hh, mm, ss,ms=0):
        self.stopdatetime = datetime.datetime(year, month, day, hh, mm, ss,ms)
        self.stopTime = self.stopdatetime

    def getPmData(self):
        data = []
        data.extend(self.__pmdata)
        self.__pmdata.clear()
        return data

    def setTopology(self, data):
        self.topologyData = data

    def getTopology(self):
        return self.topologyData

    def setSrop(self, srop):
        self.srop = srop

    def getNetId(self):
        return self.topologyData['topology']["networkId"]

    def postPmdata(self, _id, data):
        # url = "http://127.0.0.1:7000/ndp/v1/es/pmdata"
        ip = self.config["ndpip"]  # '127.0.0.1'
        port = self.config["ndpport"]  # '7000'
        sdg_path = self.config["postkpijsonpath"]  # '/ndp/v1/stopdatageneration'
        url = 'http://' + ip + ':' + port + sdg_path
        body = {}
        body['id'] = _id
        body['data'] = json.dumps(data)  # json.dumps(data)
        requests.post(url, json=body)

    def __init__(self, path, df=pd.DataFrame()):
        self.filepath = path
        self.topologyData = json.loads(path)
        #print(self.topologyData)
        self.csvflag = 0
        self.networkId=self.topologyData["topology"]["networkId"]#self.getNetId() #modify
        self.config = self.loadTemplate(configpath)
        print(self.config)
        self.datafromcsv = df
        if bool(self.config):
            try:
                self.__inputType = self.config["flag"]
                self.csvRop = self.config["rop"]
                if self.__inputType == "CSV":
                    #print(espmpath,espmonlycovpath,configpath)
                    print(configpath,kgppmpath)
                    self.datafromcsv = self.loadcsv(kgppmpath)
#                     self.datafromcoverage = self.loadcsv(espmonlycovpath)
                    if (self.datafromcsv.empty):
                        print("One of the csv file is empty ")
            except NameError as n:
                print(f"{n}, \n the above error raised while running ")
            except FileNotFoundError as ff:
                print(f"{ff} \n file not found")
        else:
            self.__inputType = "RANDOM"

            
    def loadTemplate(self, fileName):
        template = {}
        try:
            with open(fileName) as f:
                template = json.load(f)
        except Exception as e:
            print(f"Exception occured {e}, Re-check the file name ")
        return template

    def loadcsv(self, file):
        try:
            df = pd.read_csv(file,dtype={"id": "string", "endTimestamp": "string", "reportingPeriod":"string"})
            #df = df.astype(str)
            return df
        except:
            print("csv file not found")

    def generateData(self):
        if self.__inputType == "RANDOM":
            self.generateDataJson()
        elif self.__inputType == "CSV":
            self.generateJsonFromCsv(self.datafromcsv.copy())
        else:
            print("Bad Input in config")
            
    def generateData1(self):
        if self.__inputType == "RANDOM":
            self.generateDataJson()
        elif self.__inputType == "CSV":
            self.generateJsonFromCsv(self.datafromcsv.copy())
        else:
            print("Bad Input in config")

    def stopcondition(self, _id):
        networkid = {}
        networkid["networkId"] = _id
        ip = self.config["ndpip"]  # '127.0.0.1'
        port = self.config["ndpport"]  # '7000'
        sdg_path = self.config["stopdatagenerationpath"]  # '/ndp/v1/stopdatageneration'
        url = 'http://' + ip + ':' + port + sdg_path
        resp = requests.post(url, json=networkid)
        return resp

    def generateJsonFromCsv(self, df_json):
        

        initialtime = datetime.datetime.strptime(df_json["endTimestamp"].loc[df_json.index[0]],"%Y-%m-%dT%H:%M:%S")
        lastrowtime = datetime.datetime.strptime(df_json.iloc[-1].endTimestamp,"%Y-%m-%dT%H:%M:%S")
        if (self.startTime < initialtime):
            print("No data for given time range")
            print(self.startTime)
            print(initialtime)
            print(f'CSV data contains data from StartTime: {self.config["starttime"]} to EndTime: {self.config["endtime"]}')
            self.stopcondition(self.networkId)
            return None

        if (self.stopTime > lastrowtime):
            print("No data for given time range")
            print(self.stopTime)
            print(lastrowtime)
            print(f'CSV data contains data from StartTime: {self.config["starttime"]} to EndTime: {self.config["endtime"]}')
            self.stopcondition(self.networkId)
            return None

        self.logTime = self.currenttime
        nextropupdate = self.logTime + datetime.timedelta(seconds=self.csvRop)
        nextrop = nextropupdate.isoformat()
        current = self.logTime.isoformat()
        timestampCheck=list(df_json[df_json['endTimestamp']==current].index.values)
        if not bool(timestampCheck):
            print("Data not found for this time stamp" + current)
            self.stopcondition(self.networkId)
            return None

        #df_json = df_json[df_json["endTimestamp"].between(current, nextrop, inclusive="left")]
        df_json = df_json[(df_json["endTimestamp"]>=current) & (df_json["endTimestamp"]< nextrop)]

        dataList = []
        if (self.currenttime > self.stopTime):
            print("calling stopgeneration function")
            self.stopcondition(self.networkId)
        else:
            try:
                data = []
                temp = {}
                networkdata = {}
                networkdata["messageType"] = "PM"
                networkdata["networkType"] = self.networkId.split('-')[0]
                networkdata["networkId"] =self.networkId 
                networkdata["nodeType"] ="Enodeb" #if self.networkId.split('-')[0] == "LTE" else "Gnodeb"   #df_json["nodeType"].loc[df_json.index[0]]
                networkdata["iocType"] = "Cell"
                networkdata["useCase"] = "RANAuto"
                networkdata["sequence"] = 0
                for index, row in df_json.iterrows():
                    rowDict = row
                    sourceDict = {}
                    #sourceKeys = ["id", "name"]
                    sourceKeys = ["id"]
                    kpiKeys = ["endTimestamp", "reportingPeriod", "accessibility", "retainability",
                               "avgDlUeThroughput", "backhaulUtilization"]
                    kpis = dict(zip(kpiKeys, list(rowDict.get(kpiKeys))))
                    kpis["endTimestamp"]=kpis["endTimestamp"]+'Z'
                    sourceDict = dict(zip(sourceKeys, list(rowDict.get(sourceKeys))))
                    

                    #sourceDict["id"] = str(sourceDict["id"])
                    sourceDict["id"] = str(sourceDict["id"])
                    sourceDict["name"] = str(sourceDict["id"])
                    sourceDict["pmData"] = kpis
                    data.append(sourceDict)
                temp = networkdata.copy()
                temp["data"] = data
                dataList.append(temp)
                print(temp)
                self.__pmdata.append(json.dumps(temp))
                self.postPmdata(networkdata["networkId"], self.__pmdata) #modify
                self.__pmdata.clear()
                
            except Exception as esp:
                print("Error found recheck the parameters,Error is " + str(esp))
        self.currenttime = nextropupdate

    def generateDataJson(self):

        print("data")
        topology = self.topologyData
        if (self.currenttime > self.stopTime):
            print("calling stopgeneration function")
            self.stopcondition(self.networkId)
        if topology:
            try:
                RANAuto.pmDataGlobal["messageType"] = "PM"
                RANAuto.pmDataGlobal["networkId"]=topology["topology"]["networkId"]
                RANAuto.pmDataGlobal["networkType"]=topology["topology"]["networkType"]
                RANAuto.pmDataGlobal["useCase"]="RANAuto"
                RANAuto.pmDataGlobal["iocType"]="Cell"
                RANAuto.pmDataGlobal["nodeType"]="Enodeb"
                RANAuto.pmDataGlobal["sequence"] = self.sequence
                self.logTime = self.currenttime
                data=[]
                for enodebs in topology["topology"]["enodebs"]:
                    for cells in enodebs["cells"]:
                        cellData={}
                        cellData["id"]=cells["cellId"]#enodebs["enodebId"]
                        cellData["name"] = cells["cellId"]
                        #cellData["cellId"] = cells["cellId"]
                        cellData["sequence"]=0
                        looppmdata={}
                        looppmdata["endTimestamp"]=str(self.logTime.isoformat()+'Z')
                        looppmdata["reportingPeriod"]=str(self.granularity)
                        looppmdata["accessibility"]=str(random.randint(98,100))
                        looppmdata["retainability"]=str(random.randint(98,100))
                        looppmdata["avgDlUeThroughput"]=str(round(random.uniform(10240,50000),5))
                        looppmdata["backhaulUtilization"]=str(round(random.uniform(0,50),5))
                        cellData["pmData"]=looppmdata
                        data.append(cellData)
                RANAuto.pmDataGlobal["data"]=data
            except KeyError as k:
                print(f"{k} is missing or not present in the json ")
            self.__pmdata.append(json.dumps(RANAuto.pmDataGlobal))  # for testing
            print(json.dumps(RANAuto.pmDataGlobal))
            self.postPmdata(RANAuto.pmDataGlobal["networkId"], self.__pmdata) #modify
            self.__pmdata.clear()
            RANAuto.pmDataGlobal = {}
        else:
            print("Empty Topology.. Please Provide the Topology Json")

        self.logTime = self.currenttime+datetime.timedelta(seconds=self.granularity)
        self.currenttime = self.logTime
        #self.sequence += 1

    def createJson(self):
        if len(self.__pmdata) == 0:
            print("No pmdata is generated, run -->generateData(\"RANDOM\")<-- to generate data")
            return ""
        with open("pmdataes.json", "w") as ee:
            json.dump(self.__pmdata, ee, indent=4, sort_keys=False)

            
    def pmdataprint(self):

        print(json.dumps(self.__pmdata))

        

    def pmconfig(self):

        print(self.config)

