#!/bin/bash 
sudo apt-get install build-essential libpq-dev libssl-dev openssl libffi-dev zlib1g-dev 
sudo apt-get install python3-pip python3.7-dev
sudo apt-get install python3.7
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
sudo apt-get install python3-apt
python3 -m pip install --upgrade pip
pip3 -r install /home/sterlite/nikita/sandbox/Sandbox/requirements.txt
