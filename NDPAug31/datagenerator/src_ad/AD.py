from datetime import timezone
from random import sample
import datetime
import os
import random
import json
import sys
import time
import string
import requests

path1=os.getcwd()
mypath=os.path.abspath(path1)
configpath=os.path.join(mypath,"datagenerator/config.json")
class AD:
    pmDataGlobal={}
    granularity=0
    __pmdata=[]
    netid = ""

    def setGranularity(self,rop):
        self.granularity = rop

    def setSrop(self, srop):
        self.srop = srop

    def getPmData(self):
        data = []
        data.extend(self.__pmdata)
        self.__pmdata.clear()
        print(data)
        return data

    def setStartDateTime(self,year,month,day,hh,mm,ss):
        self.startdatetime = datetime.datetime(year,month,day,hh,mm,ss)
        self.startTime = self.startdatetime
        self.currenttime = self.startTime

    def setStopDateTime(self, year, month, day, hh, mm, ss):
        self.stopdatetime = datetime.datetime(year, month, day, hh, mm, ss)
        self.stopTime = self.startdatetime

    def setTopology(self, data):
        self.topologyData = data

    def getTopology(self):
        return self.topologyData

    def postPmdata(self,id,data):
         url = "http://127.0.0.1:7000/ndp/v1/post/pmdata"
         body={}
         body['id']=id
         body['data']=json.dumps(data)
         requests.post(url,json=body)

    def stopcondition(self, _id):
        networkid = {}
        networkid["networkId"] = _id
        ip = self.config["ndpip"]  # '127.0.0.1'
        port = self.config["ndpport"]  # '7000'
        sdg_path = self.config["stopdatagenerationpath"]  # '/ndp/v1/stopdatageneration'
        url = 'http://' + ip + ':' + port + sdg_path
        # url='http://127.0.0.1:7000/ndp/v1/stopdatageneration'

        resp = requests.post(url, json=networkid)
        return resp

    def getNetId(self):
        return self.topologyData["networkId"]


    def __init__(self,topology):
        
        #self.topologyData=self.loadTemplate("ESTopology.json")
        #self.topologyData=self.loadTemplate(topology)
        self.topologyData = json.loads(topology)
        self.config = self.loadTemplate(configpath)


        
        
    def loadTemplate(self,fileName):
        template={}
        with open(fileName) as f:
            template=json.load(f)
        return template
    
    
    
    def generateData(self):
        
        
        topology=self.topologyData
        self.logtime = self.currenttime
        if topology:
            
            try:
                
                dataList=[]
                AD.pmDataGlobal["networkType"]=topology["networkType"]
                AD.pmDataGlobal["networkId"]=topology["networkId"]
                AD.pmDataGlobal["nodeType"]="DU"
                AD.pmDataGlobal["iocType"]="Cell"
                AD.pmDataGlobal["useCase"]="AD"
                AD.pmDataGlobal["sequence"]=1
                
                for du in topology["topology"]:
                    if(du["device"]["type"]=="du"):
                        for cell in (du["device"]["attributes"]["cells"]):
                            #currentTime = datetime.datetime.now(tz=timezone.utc)
                            self.logTime = self.currenttime
                            #self.logTime = self.currenttime+datetime.timedelta(seconds=self.granularity);
                            pmdict={}
                            pmdict["cellId"]=cell["cellId"]
                            
                            pmdata={}

                            #pmdata["pmTimestamp"]=currentTime.isoformat() + 'Z'
                            pmdata["endTimestamp"]=str(self.logTime.isoformat()+'Z')
                            pmdata["reportingPeriod"]=str(self.granularity)
                            pmdata["avgUlInterferenceAllPrbs"]=str(random.randint(20,100))
                            pmdata["avgUlInterferencePerPrb"]=str(random.sample(range(20, 100), random.randint(4,10)))
                            pmdata["avgInterferencePerSymbolUpPTS"]=str(random.sample(range(20, 100), random.randint(4,10)))
                            pmdata["avgInterferencePerSymbolFirstUlSubframe"]=str(random.sample(range(20, 100), random.randint(4,10)))
                            pmdata["NumberOfDlPrbsAvailable"]=str(random.randint(10,600))
                            pmdata["NumberOfDlPrbsUsed"]=str(random.randint(10,pmdata["NumberOfDlPrbsAvailable"]))
                            pmdict["pmData"]=pmdata
                            
                            dataList.append(pmdict)
                            
                AD.pmDataGlobal["data"]=dataList
                
                
            except KeyError as k:
                print(f"{k} is missing or not present in the json ")
            self.logTime = self.currenttime + datetime.timedelta(seconds=self.granularity);
            self.__pmdata.append(json.dumps(AD.pmDataGlobal))
            self.postPmdata(AD.pmDataGlobal["networkId"], self.__pmdata)
            self.__pmdata.clear()
           # print(json.dumps(AD.pmDataGlobal))

        else:
            return ("Empty Topology.. Please Provide the Topology Json")

        self.currenttime = self.logTime
