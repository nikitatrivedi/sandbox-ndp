from datetime import timezone
from random import sample
import datetime
import random
import json
import sys, os
import time
import string
import pandas as pd
import requests
from urllib3.util import current_time
from SandboxNdp import stopdatageneration

# from backports.datetime_fromisoformat import MonkeyPatch
# p = os.path.abspath('..')
# sys.path.insert(1, p)


#mypath=os.path.abspath(os.path.dirname(__file__))
mypath=os.path.dirname(__file__)
configpath = os.path.join(mypath, "config.json")
espmpath = os.path.join(mypath, "ESPMCSV.csv")
espmonlycovpath = os.path.join(mypath, "ESPMCSV_only_coverage.csv")


#path1 = os.getcwd()
#mypath = os.path.abspath(path1)
#configpath = os.path.join(mypath, "datagenerator/config.json")
#espmpath = os.path.join(mypath, "datagenerator/ESPMCSV.csv")
#espmonlycovpath = os.path.join(mypath, "datagenerator/ESPMCSV_only_coverage.csv")

# espmonlycovpath="ESPM_only_coverage.csv"
class energySaving:
    cellStateStore = {}
    pmDataGlobal = {}
    __inputType = ""
    netid = ""
    sequence = 1
    csvRop = 0
    __allinOneData = []
    __pmdata = []
    flag = False
    filepath = ""
    cmCellState = {}

    def setGranularity(self, rop):
        self.granularity = rop
        return self.granularity

    def getCellId(self, cellId):
        self.cellId = cellId

    def setStartDateTime(self, year, month, day, hh, mm, ss, ms=0):
        self.startdatetime = datetime.datetime(year, month, day, hh, mm, ss, ms)
        self.startTime = self.startdatetime
        self.currenttime = self.startTime

    def setStopDateTime(self, year, month, day, hh, mm, ss, ms=0):
        self.stopdatetime = datetime.datetime(year, month, day, hh, mm, ss, ms)
        self.stopTime = self.stopdatetime

    def getPmDataOneGo(self):
        tempdata = self.__allinOneData.copy()
        self.__allinOneData.clear()
        return tempdata

    def getPmData(self):
        data = []
        data.extend(self.__pmdata)
        self.__pmdata.clear()
        print(type(data))
        print(data)
        return data

    def triggerCMupdate(self, cmNotification):
        print("hello cm notification")
        print(cmNotification)
        self.flag = True
        removingcells = []
        for key in cmNotification["data"]:
            print(key)
            #cmNotification[key["id"]] = key["attributes"][1]["energySavingState"]
            cmNotification[key["id"]] = key["attributes"][0]["energySavingState"]

            # print(cmNotification[key["id"]])
            # self.cmCellState[key["id"]]=key["attributes"][1]["energySavingState"]
            if cmNotification[key["id"]] == "isEnergySaving":
                self.cmCellState[key["id"]] = cmNotification[key["id"]]
            else:
                if key["id"] in self.cmCellState:
                    removingcells.append(self.cmCellState[key["id"]])
                    del self.cmCellState[key["id"]]
                else:
                    print("cellId Not present")
        for du in self.topologyData["topology"]:
            if du["device"]["type"] == "du":
                for cell in du["device"]["attributes"]["cells"]:
                    if cell["cellId"] in self.cmCellState.keys():
                        if self.cmCellState[cell["cellId"]] == "isEnergySaving":
                            cell["energySavingControl"] = "toBeEnergySaving"
                            cell["energySavingState"] = "isEnergySaving"
                    elif cell["cellId"] in removingcells:
                        cell["energySavingControl"] = "toBeNotEnergySaving"
                        cell["energySavingState"] = "isNotEnergySaving"
                    else:
                        pass

    def setTopology(self, data):
        self.topologyData = data

    def getTopology(self):
        return self.topologyData

    def setSrop(self, srop):
        self.srop = srop

    def getNetId(self):
        return self.topologyData["networkId"]

    def postPmdata(self, _id, data):
        # url = "http://127.0.0.1:7000/ndp/v1/es/pmdata"
        ip = self.config["ndpip"]  # '127.0.0.1'
        port = self.config["ndpport"]  # '7000'
        sdg_path = self.config["postkpijsonpath"]  # '/ndp/v1/stopdatageneration'
        url = 'http://' + ip + ':' + port + sdg_path
        body = {}
        body['id'] = _id
        body['data'] = json.dumps(data)  # json.dumps(data)
        requests.post(url, json=body)

    def __init__(self, path, df=pd.DataFrame()):

        self.filepath = path
        self.topologyData = json.loads(path)
        self.csvflag = 0
        self.config = self.loadTemplate(configpath)
        self.csvRop = self.config["rop"]
        self.datafromcsv = df
        self.datafromcoverage = df
        self.cmCellState = {}
        if bool(self.config):
            try:
                self.__inputType = self.config["flag"]
                if self.__inputType == "CSV":
                    print(espmpath, espmonlycovpath, configpath)

                    self.datafromcsv = self.loadcsv(espmpath)
                    self.datafromcoverage = self.loadcsv(espmonlycovpath)
                    if (self.datafromcsv.empty or self.datafromcoverage.empty):
                        print("One of the csv file is empty ")

            except NameError as n:
                print(f"{n}, \n the above error raised while running ")
            except FileNotFoundError as ff:
                print(f"{ff} \n file not found")

        else:
            self.__inputType = "RANDOM"

    def loadTemplate(self, fileName):
        template = {}
        try:
            with open(fileName) as f:
                template = json.load(f)
        except Exception as e:
            print(f"Exception occured {e}, Re-check the file name ")
        return template

    def loadcsv(self, file):
        try:
            df = pd.read_csv(file, dtype={"id": "string", "endTimestamp": "string", "reportingPeriod": "string",
                                          "energySavingState": "string"})
            df = df.astype(str)
            return df

        except:
            print("csv file not found")
        # return datacsv

    def generateData(self):
        if self.__inputType == "RANDOM":
            self.generateDataJson()
        elif self.__inputType == "CSV":
            self.newgenerateJsonFromCsv(self.datafromcsv.copy(), self.datafromcoverage.copy())
        else:
            print("Bad Input in config")

    def stopcondition(self, _id):
        networkid = {}
        networkid["networkId"] = _id
        ip = self.config["ndpip"]  # '127.0.0.1'
        port = self.config["ndpport"]  # '7000'
        sdg_path = self.config["stopdatagenerationpath"]  # '/ndp/v1/stopdatageneration'
        url = 'http://' + ip + ':' + port + sdg_path
        # url='http://127.0.0.1:7000/ndp/v1/stopdatageneration'

        resp = requests.post(url, json=networkid)
        return resp

    def newgenerateJsonFromCsv(self, df_json, df_json1):

        initialtime = datetime.datetime.strptime((df_json["endTimestamp"].loc[df_json.index[0]])[:-1], "%Y-%m-%dT%H:%M:%S")
        lastrowtime = datetime.datetime.strptime((df_json.iloc[-1].endTimestamp)[:-1], "%Y-%m-%dT%H:%M:%S")
        # csvnetid = df_json["networkId"].loc[df_json.index[0]]
        csvnetid = self.getNetId()

        if (self.startTime < initialtime):
            print("No data for given time range")
            print(self.startTime)
            print(initialtime)
            print(
                f'CSV data contains data from StartTime: {self.config["starttime"]} to EndTime: {self.config["endtime"]}')
            self.stopcondition(csvnetid)

            return None
        if (self.stopTime > lastrowtime):
            print("No data for given time range")
            print(self.stopTime)
            print(lastrowtime)
            print(
                f'CSV data contains data from StartTime: {self.config["starttime"]} to EndTime: {self.config["endtime"]}')
            self.stopcondition(csvnetid)
            return None

        self.logTime = self.currenttime
        nextropupdate = self.logTime + datetime.timedelta(seconds=self.csvRop)
        nextrop = nextropupdate.isoformat()
        current = self.logTime.isoformat()

        #df_json = df_json[df_json["endTimestamp"].between(current, nextrop, inclusive="left")]
        df_json = df_json[(df_json["endTimestamp"]>=current) & (df_json["endTimestamp"]< nextrop)]
        #df_json1 = df_json1[df_json1["endTimestamp"].between(current, nextrop, inclusive="left")]
        df_json1 = df_json1[(df_json1["endTimestamp"]>=current) & (df_json1["endTimestamp"]< nextrop)]

        df_ = pd.DataFrame()
        # if self.flag:
        #     self.flag  = False
        # print(df_json1)
        if bool(self.cmCellState):
            # if self.flag:
            print("Update Triggered!!!!!")

            self.flag = False
            # cccell=pd.DataFrame()

            for item in self.cmCellState:
                capcell = covcell = pd.DataFrame()
                # if self.cmCellState[item]=="isEnergySaving":
                print(self.cmCellState)

                capcell = df_json1.loc[df_json1["id"] == item]
                df_ = pd.concat([df_, capcell])
                covcell = df_json1.loc[capcell.index[0] - 1].to_frame().T  # .iloc[capcell.index-1]
                df_ = pd.concat([df_, covcell])

            print(df_)
            df_json.loc[df_.index, :] = df_[:]
        dataList = []

        if (self.topologyData["networkId"] != csvnetid):
            print(
                f"Network Id Not matched  \n Asked for NetworkId : {self.topologyData['networkId']} \n CSV's Network Id: {csvnetid} ")
            return csvnetid
            # self.stopcondition(networkdata["networkId"])

        if (self.currenttime > self.stopTime):
            print("calling stopgeneration function")
            self.stopcondition(csvnetid)
        else:
            try:
                data = []
                temp = {}
                networkdata = {}
                networkdata["messageType"] = df_json["messageType"].loc[df_json.index[0]]
                # networkdata["networkType"] = df_json["networkType"].loc[df_json.index[0]]
                networkdata["networkType"] = csvnetid.split('-')[0]

                # networkdata["networkId"] = df_json["networkId"].loc[df_json.index[0]]
                networkdata["networkId"] = csvnetid

                networkdata["nodeType"] = df_json["nodeType"].loc[df_json.index[0]]
                networkdata["iocType"] = df_json["iocType"].loc[df_json.index[0]]
                networkdata["useCase"] = df_json["useCase"].loc[df_json.index[0]]
                networkdata["sequence"] = int(df_json["sequence"].loc[df_json.index[0]])
                for index, row in df_json.iterrows():
                    rowDict = row
                    sourceDict = {}

                    sourceKeys = ["id", "name"]
                    kpiKeys = ["endTimestamp", "reportingPeriod", "energySavingState", "prbTotDl",
                               "dlCellTrafficVolume", "numOfActiveUEs", "radioEnergyConsumption", "accessibility"]
                    kpis = dict(zip(kpiKeys, list(rowDict.get(kpiKeys))))
                   # kpis["endTimestamp"] = kpis["endTimestamp"] + 'Z'
                    kpis["endTimestamp"] = kpis["endTimestamp"]

                    sourceDict = dict(zip(sourceKeys, list(rowDict.get(sourceKeys))))
                    sourceDict["pmData"] = kpis
                    sourceDict["id"] = str(sourceDict["id"])
                    data.append(sourceDict)

                temp = networkdata.copy()
                temp["data"] = data
                dataList.append(temp)
                self.__pmdata.append(json.dumps(temp))
                self.postPmdata(networkdata["networkId"], self.__pmdata)
                self.__pmdata.clear()
                self.__allinOneData.append(json.dumps(temp))
            except Exception as esp:
                print("Error found recheck the parameters,Error is " + str(esp))
        self.currenttime = nextropupdate

    def generateDataCsv(self):
        print("JSON to csv")
        try:
            with open("pmdataes.json") as ff:
                data = json.load(ff)
        except Exception as e:
            print("file not found, raised expection" + str(e))

        df = pd.json_normalize(data)
        df = df[["messageType", "networkType", "networkId", "nodeType", "iocType", "useCase", "sequence"]]

        ll = []
        for i in data:
            dffor = pd.json_normalize(i["data"])
            ll.append(dffor)
        kpi = pd.concat(ll, ignore_index=True)
        dic = df.to_dict('records')

        colum = list(df.columns)
        # coll=list(dffor.columns)
        colum.extend(list(dffor.columns))  # coll)

        cc = []
        for i in colum:
            k = i.split(".")
            if (len(k)) == 2:
                cc.append(k[1])
            else:
                cc.append(k[0])

        df_m = pd.concat([df, kpi], axis=1)

        for key, value in dic[0].items():
            df_m[key] = value

        df_m.columns = cc
        df_m["id"] = df_m["id"].astype(str)
        df_m.to_csv("ESPMCSV.csv", index=False)

    def generateDataJson(self):

        if self.flag:
            print("Update Triggered!!!!!")
            # fetch the dictionary and

            self.flag = False

        topology = self.topologyData

        # self.logtime = self.currenttime

        if topology:
            try:
                dataList = []
                energySaving.pmDataGlobal["messageType"] = "PM"
                energySaving.pmDataGlobal["networkType"] = topology["networkType"]
                energySaving.pmDataGlobal["networkId"] = topology["networkId"]
                energySaving.pmDataGlobal["nodeType"] = "CUCP"
                energySaving.pmDataGlobal["iocType"] = "Cell"
                energySaving.pmDataGlobal["useCase"] = "ES"
                energySaving.pmDataGlobal["sequence"] = self.sequence
                self.logTime = self.currenttime
                # self.logTime = self.currenttime + datetime.timedelta(seconds=self.granularity)

                for du in topology["topology"]:
                    if (du["device"]["type"] == "du"):
                        for cell in (du["device"]["attributes"]["cells"]):
                            # currentTime = datetime.datetime.now(tz=timezone.utc)
                            # self.logTime = self.currenttime+datetime.timedelta(seconds=self.granularity)
                            pmdict = {}
                            pmdict["id"] = cell["cellId"]
                            pmdict["name"] = cell["cellName"]
                            pmdata = {}

                            # pmdata["endTimestamp"] = self.logTime.isoformat()
                            pmdata["endTimestamp"] = str(self.logTime.isoformat() + 'Z')
                            pmdata["reportingPeriod"] = str(self.granularity)
                            pmdata["energySavingState"] = str(cell["energySavingState"])
                            if cell["cellId"] in self.cmCellState and self.cmCellState[
                                cell["cellId"]] == "isEnergySaving":
                                # if cell["energySavingState"] == "isEnergySaving":
                                pmdata["prbTotDl"] = pmdata["dlCellTrafficVolume"] = pmdata["numOfActiveUEs"] = str(0)
                                pmdata["radioEnergyConsumption"] = str(120)
                                pmdata["accessibility"] = str(0)
                                # pmdata["nbrList"]=[]

                            else:
                                pmdata["prbTotDl"] = str(random.randint(50, 100))
                                pmdata["dlCellTrafficVolume"] = str(random.randint(100, 5000))
                                pmdata["numOfActiveUEs"] = str(random.randint(10, 500))
                                pmdata["radioEnergyConsumption"] = str(270)
                                pmdata["accessibility"] = str(random.uniform(90, 100))
                                # pmdata["nbrlist"]=[i["nRTCI"] for i in cell["neighbours"]]

                            pmdict["pmData"] = pmdata
                            dataList.append(pmdict)
                energySaving.pmDataGlobal["data"] = dataList
            except KeyError as k:
                print(f"{k} is missing or not present in the json ")

            self.__pmdata.append(json.dumps(energySaving.pmDataGlobal))  # for testing
            # self.__pmdata.append(energySaving.pmDataGlobal)
            self.postPmdata(energySaving.pmDataGlobal["networkId"], self.__pmdata)
            self.__pmdata.clear()
            energySaving.pmDataGlobal = {}

        else:
            print("Empty Topology.. Please Provide the Topology Json")

        self.logTime = self.currenttime + datetime.timedelta(seconds=self.granularity)
        self.currenttime = self.logTime
        self.sequence += 1

    def createJson(self):
        if len(self.__pmdata) == 0:
            print("No pmdata is generated, run -->generateData(\"RANDOM\")<-- to generate data")
            return ""
        with open("pmdataes.json", "w") as ee:
            json.dump(self.__pmdata, ee, indent=4, sort_keys=False)

    def pmdataprint(self):
        print(json.dumps(self.__pmdata))
