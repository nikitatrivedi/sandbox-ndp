import os
import threading
import sys
#from tkinter import Radiobutton
from dateutil.parser import parse
sys.path.insert(0, '..')

from flask import Flask, jsonify, request
from flask import Flask, session
import json
import time

app = Flask(__name__)
__ndp = None
ndpdict = {'ndpobj': None, 'ndpNetid':None, 'rop':0, 'ndpthread': None, 'ndpthreadstate': False}
ndpObjList = {}

def workerthread(tempdict, rop):
    __ndp = tempdict['ndpobj']
    __ndp.generateData()
    currentTime = time.time()
    itertime = time.time()
    while (tempdict['ndpthreadstate']):
        if (currentTime - itertime >= rop):
            print("Global Dict Worker-- > ", ndpObjList)
            __ndp.generateData()
            itertime = time.time()
            print("worker",tempdict['ndpNetid'])
            #time.sleep(rop)
        currentTime = time.time()

@app.route('/initiateusecase', methods=['GET','POST'])
def initiateusecase():
    tempdict={}
    query = request.get_json()
    jsonbody = json.dumps(query)
    usecase = json.loads(jsonbody)["usecase"]
    path = json.loads(jsonbody)["topologyJson"]

    message = "Data Generator Instantiated"
    if usecase == "ES":
        from src_es.energySaving import energySaving
        __ndp = energySaving(path)
        tempdict['ndpobj'] = __ndp

    if usecase == "RanAuto":
        from src_ranauto.RANAuto import RANAuto
        __ndp = RANAuto(path)
        tempdict['ndpobj'] = __ndp

    if usecase == "AD":
        from src_ad.AD import AD
        __ndp = AD(path)
        tempdict['ndpobj'] = __ndp

    tempdict['ndpNetid'] = __ndp.getNetId()
    ndpObjList[__ndp.getNetId()] = tempdict
    print("Temp Dict Initiate-- > ", tempdict)
    #ndpObjList.append({__ndp.getNetId():tempdict})
    return jsonify({'Usecase': usecase, "Message": "UseCase Instantiated Successfully"})


@app.route('/startdatageneration', methods=['GET','POST'])
def startdatageneration():
    query = request.get_json()
    jsonbody = json.dumps(query)
    rop = json.loads(jsonbody)["rop"]
    srop = json.loads(jsonbody)["srop"]
    netid = json.loads(jsonbody)["netId"]

    tempdict = ndpObjList[netid]
    print("Temp Dict Startusecase-- > ", tempdict)
    print("Global Dict Startusecase-- > ", ndpObjList)

    tempdict['rop'] = int(rop)
    tempdict['srop'] = int(srop)
    __rop = int(rop)

    starttime = json.loads(jsonbody)["starttime"]
    stoptime = json.loads(jsonbody)["stoptime"]
    dateobj = parse(starttime)
    stopdateobj = parse(stoptime)
    __ndp = tempdict['ndpobj']

    __ndp.setGranularity(int(rop))
    __ndp.setSrop(int(srop))
    __ndp.setStartDateTime(dateobj.year,dateobj.month,dateobj.day,dateobj.hour,dateobj.minute,dateobj.second)
    __ndp.setStopDateTime(stopdateobj.year,stopdateobj.month,stopdateobj.day,stopdateobj.hour,stopdateobj.minute,stopdateobj.second)
    #__ndp.generateData()
    rop = tempdict['rop']
    #tempdict['ndpthread'] = threading.Timer(rop, workerthread, args=(__ndp, rop))
    #tempdict['ndpthread'] = threading.Timer(rop, workerthread, args=(tempdict, rop))
    tempdict['ndpthread'] = threading.Timer(0, workerthread, args=(tempdict, srop))


    tempdict['ndpthreadstate'] = True
    __threadId = tempdict['ndpthread']
    __threadId.start()
    ndpObjList[netid] = tempdict
    print("Temp Dict end Startusecase -- > ", tempdict)
    print("startdg", __threadId)
    return "Data Generator Started"


@app.route('/stopdatageneration/<string:netid>', methods=['GET', 'POST'])
def stopdatageneration(netid):
    tempdict = ndpObjList[netid]
    __ndp = tempdict['ndpobj']
    __threadId = tempdict['ndpthread']
    tempdict['ndpthreadstate'] = False
    print(__threadId)
    __threadId.cancel()
    ndpObjList[netid]=tempdict
    return "Data Generator Stopped"


@app.route('/deletedatagenerator/<string:netid>', methods=['GET', 'POST'])
def deletedatageneration(netid):
    tempdict = ndpObjList[netid]
    print("delete before->",ndpObjList)
    __ndp = tempdict['ndpobj']
    __threadId = tempdict['ndpthread']
    tempdict['ndpthreadstate'] = False
    print(__threadId)
    __threadId.cancel()
    __threadId.join()
    #ndpObjList[netid]=tempdict
    del ndpObjList[netid]
    print("delete after-> ", ndpObjList)
    return "Data Generator killed"


@app.route('/getpmkpidata/<string:netid>', methods=['GET', 'POST'])
def pmkpidata(netid):
    if (request.method == 'GET'):
        tempdict = ndpObjList[netid]
        print("Temp Dict pm kpi-- > ", tempdict)
        __ndp = tempdict['ndpobj']
        data = __ndp.getPmData()
        return json.dumps(data, indent=2)

@app.route('/triggerconfigchange/<string:netid>', methods=['GET','POST'])
def triggercmupdate(netid):
    tempdict = ndpObjList[netid]
    query = request.get_json()
    jsonbody = json.dumps(query)
    __ndp = tempdict['ndpobj']
    print("updating config",tempdict['ndpNetid'])
    newtopo = json.loads(jsonbody)
    __ndp.triggerCMupdate(newtopo)
    #__ndp.setTopology(newtopo)
    return "flag updated"

# driver function
if __name__ == '__main__':
    # usecase = sys.argv[1]
    # __ndp=None
    app.run(host='0.0.0.0', debug=True, use_reloader=False)
